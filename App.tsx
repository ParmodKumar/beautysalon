import * as React from 'react';
import {Text,ImageBackground,StyleSheet, View,Image} from 'react-native';
import SplashScreen from 'react-native-splash-screen'
import {Navigator} from './src/utills/Navigations/Navigator'

    interface Props{
        }
    interface State{  
    }
    export class App extends React.Component<Props,State>{
    
        constructor(props) {
          super(props);
        }
        componentDidMount() {
            setTimeout(() => SplashScreen.hide(),2500);
        }
        render(){
          return <Navigator />
        }
    }
   
