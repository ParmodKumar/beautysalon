import * as yup from 'yup';


export class  Validator {
      static Longvalidation (){
          return yup.object().shape({
            NameInuputValue : yup.string().required('Not is Valid FristNameInuputValue'),
            emailInuputValue : yup.string().email('Not is Valid e-email').required('Eamil is required'),
            PasswordInuputValue:  yup.string().required("No Password Provied PasswordInuputValue").max(3 ,'Password should not excced 3 chars.')
          })
      }
}


export const validateLoginForm = data => {
 
  return new Promise(resolve => {
    let success = true;
    let message = '';
    if (!data.mobile) {
      success = false;
      message = 'Please add mobile number';
    } else if (data.mobile.length < 10) {
      success = false;
      message = 'Please add valid mobile number';
    }
    resolve({
      status: success,
      message: message,
    });
  });
};
