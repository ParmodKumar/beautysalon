import * as React from 'react';
import {Platform,ViewStyle,TextStyle,ImageStyle, Dimensions} from 'react-native';

    export class Utils{
        static dynamStyle(
            portraiStyle: 
        |   ImageStyle
        |   ImageStyle[],
            landScapeStyle:
        |   ImageStyle 
        |   ImageStyle[],
            orientation:string 
            ){
        return   orientation === "portrait" ? portraiStyle:landScapeStyle
}
        static image = {
            Background : require('../assets/backgroundimg.jpg'),
            Logoimg : require('../assets/logo1.jpg'),
            Gender : require('../assets/gender.jpg'),
            Gender1 : require('../assets/gender1.jpg'),
            SingiNimg : require('../assets/signin.jpg'),
            Verify:require('../assets/Verify.jpg'),
            Map : require('../assets/map.jpg'),
            Carousel0:require('../assets/carousel0.jpg'),
            Carousel1:require('../assets/carousel1.jpg'),
            Carousel2:require('../assets/carousel2.jpg'),
            Carousel3:require('../assets/carousel3.jpg'),
            Carousel4:require('../assets/carousel4.jpg'),
            Carousel5:require('../assets/carousel5.jpg'),
            Carousel6:require('../assets/carousel6.jpg'),
            TopServices:require('../assets/TopServices1.jpg'),
            dryer:require('../assets/dryer.jpg'),
            BestSalon1:require('../assets/BestSalon1.jpg'),
            BestSalon2:require('../assets/BestSalon1.jpg'),
            BestSalon3:require('../assets/BestSalon1.jpg'),
            Haircut:require('../assets/Haircut.jpg'),
            Spa:require('../assets/Spa.jpg'),
            Coloring:require('../assets/Coloring.jpg'),
            Styling:require('../assets/Styling.jpg'),
            OrangeSalon:require('../assets/OrangeSalon.jpg'),
            Monarch:require('../assets/Monarch.jpg'),
            Gallery1:require('../assets/Gallery1.jpg'),
            Gallery2:require('../assets/Gallery2.jpg'),
            Gallery3:require('../assets/Gallery3.jpg'),
            Gallery4:require('../assets/Gallery4.jpg'),
            Gallery5:require('../assets/Gallery5.jpg'),
            Serviceshairstyle:require('../assets/Serviceshairstyle.jpg'),
            ServicesSpa:require('../assets/ServicesSpa.jpg'),
            ServicesColoring:require('../assets/ServicesColoring.jpg'),
            ServicesStyling:require('../assets/ServicesStyling.jpg'),
            MonarchSalon:require('../assets/MonarchSalon.jpg'),
            Popular1:require('../assets/Popular1.jpg'),
            Popular2:require('../assets/Popular2.jpg'),
            Popular3:require('../assets/Popular3.jpg'),
            Popular4:require('../assets/Popular4.jpg'),
            scissor:require('../assets/scissor.jpg'),
            Wax:require('../assets/Wax.jpg'),
            Margaret:require('../assets/Margaret.jpg'),
            OrangeTheSalon:require('../assets/OrangeTheSalon.jpg'),
            EnhanceSalon:require('../assets/EnhanceSalon.jpg'),
            Paymentlogo:require('../assets/paymentlogo.jpg')
        }
        static IsIos(){
          return Platform.OS === 'ios'
        }
}




