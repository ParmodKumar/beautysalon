import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import {SinnUpStack} from '../Navigations/Routes';



export const Navigator =()=> {
  return (
    <NavigationContainer>{SinnUpStack()}</NavigationContainer>
  );
}