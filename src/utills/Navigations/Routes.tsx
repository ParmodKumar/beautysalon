import * as React from 'react';
import{View,Text} from 'react-native'
import { createStackNavigator } from '@react-navigation/stack';
import SigninandSignUp from '../../Screen/SigninandSignUp';
import SingIn from '../../Screen/SingIn';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { PrimaryTheme } from '../../style/Theme';
import SingUp from '../../Screen/SingUp';
import VerifyAccount from '../../Screen/VerifyAccount';
import HairStyling from '../../Screen/HairStyling';
import Icon from '../../component/Icon';



const Stack = createStackNavigator();
  export enum ScreenName {
      SININANDSINUP = 'SigninandSignUp',
      SINGIN = 'Sing In',
      SINGUP = 'Sing Up',
      VERIFTYACCOUNT ='VerifyAccount',
      HAIRSTYLING = 'HairStyling',
  }
  export enum ScreenNames {
    HomeSCREEN ='HomeScreen',
    CARDSCREEN = 'CardScreen',
    BOOKINGSREEN= 'BookingScreen',
    FAVORITESSREEN = 'FavoritesScreen',
    MAPSREEN = 'Mapsreen',
    ViewLIST = 'Viewlist',
    TRENDINGSERVICES= 'TrendingServices'
   }
export const SinnUpStack = () =>{
    return (
        <Stack.Navigator  screenOptions={{ 
          headerShown: false, 
          }}  
          initialRouteName={ScreenName.SININANDSINUP}>
          <Stack.Screen   name={ScreenName.SININANDSINUP} component={SigninandSignUp} />
            <Stack.Screen 
                options={{ headerShown:true,
                  headerTintColor:PrimaryTheme.$TEXT_COLOR,
                  headerStyle:{
                  backgroundColor:PrimaryTheme.$BACKGROUND_COLOR,
                  height: hp('8%'),
                  },
              }} 
              name={ScreenName.SINGIN} component={SingIn} 
            />
            <Stack.Screen  options={{
                headerShown: true,
                headerTintColor:PrimaryTheme.$TEXT_COLOR,
                headerStyle:{
                backgroundColor:PrimaryTheme.$BACKGROUND_COLOR,
                height: hp('8%'),
              },
            }} name={ScreenName.SINGUP} component={SingUp} 
            />
            <Stack.Screen options={{
                headerShown: true,
                headerTintColor:PrimaryTheme.$TEXT_COLOR,
                headerStyle:{
                backgroundColor:PrimaryTheme.$BACKGROUND_COLOR,
                height: hp('8%'),
              },
              }} name={ScreenName.VERIFTYACCOUNT} component={VerifyAccount} 
            />
            <Stack.Screen options={{
              //  title:'High Rd, University',
              //  headerLeft:()=>(
              //   <View style={{marginLeft:10,}}>
              //     <Icon name={'location-outline'} size={30} color={PrimaryTheme.$BACKGROUND_COLOR} style={{marginHorizontal: 10,}}/>    
              //   </View>  
              //  ),
              //  headerTitleStyle:{color:PrimaryTheme.$TEXT_COLOR,fontSize:16},
              //   headerShown: true,
              //   headerTintColor:PrimaryTheme.$TEXT_COLOR,
              //   headerStyle:{
              //   backgroundColor:PrimaryTheme.$BACKGROUND_COLOR,
              //   height: hp('8%'),
              
              // },
              }} name={ScreenName.HAIRSTYLING} component={HairStyling}  
              
            />


          </Stack.Navigator>

      );
}
