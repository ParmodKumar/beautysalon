import * as  React from 'react'
import {Text} from 'react-native';
import Icons from 'react-native-vector-icons/Ionicons'
import {PrimaryTheme} from '../style/Theme';
import {Utils} from '../utills/utils';


interface Props{
    name: string;
    color?:string;
    size?:number;
    style?:any;
    noPrefix?:boolean;
    onPress?:any;
}
const Icon  = (props:Props)=>{
    const IconName = ()=>{
        if(Utils.IsIos){
            return 'ios-' + props.name
        }
      return 'md-' + props.name
    }
    return(
       <Icons name={props.noPrefix ? props.name : IconName()} color={props.color} size={props.size} style={props.style}/>
    )
}


Icon.defaultProps ={
    noPrefix:false,
    color:PrimaryTheme.$TEXT_COLOR,
}
export default Icon;