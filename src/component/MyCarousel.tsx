
import React, { useState, useEffect } from 'react';
import { Text, View, Animated, StyleSheet, TouchableOpacity, Pressable, FlatList, Image } from 'react-native';
import { Calendar } from 'react-native-calendars';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { images } from '../utills/Date';
import ImageOverlay from "react-native-image-overlay";
import { PrimaryTheme } from '../style/Theme';
import CustomText from '../component/CustomText';
import { landscapeTypography, Spaning, Typography } from '../style/Global';
import Container from '../component/Container';
import { useOrientation } from '../utills/useOrientation';

interface Props{
  onPress?:any
}

const MyCarousel = (props:Props) => {
  const orientation = useOrientation();

        const  renderItem = ({ item }) => (
          <TouchableOpacity onPress={props.onPress} activeOpacity={0.8}  
            style={{marginRight:Spaning.tiny.marginRight}}>
              <ImageOverlay 
                source={item.image}
                containerStyle={[orientation === 'portrait' ? MyCarouselportrait.image : MyCarousellandscape.image]}
              >
              <View>
                <CustomText style={[ orientation === 'portrait' ?  Typography.lighText : 
                landscapeTypography.lighText
                  ,{color:PrimaryTheme.$TEXT_COLOR,textAlign:"center",}]}>{item.title}</CustomText>
                <CustomText style={[orientation === 'portrait' ? Typography.title :landscapeTypography.title,
                  {color:PrimaryTheme.$TEXT_COLOR,fontSize: wp('5.3%'),}]}>{item.heading}</CustomText>
                <CustomText style={[orientation === 'portrait' ?  Typography.heading : landscapeTypography.heading,
                  {color:PrimaryTheme.$TEXT_COLOR,textAlign:"center",fontSize:wp('3.5%')}]}>{item.subheading}</CustomText>
              </View>   
            </ImageOverlay>
          </TouchableOpacity>
      );

    return (
        <FlatList showsVerticalScrollIndicator={false}
          data={images[0]}
          renderItem={renderItem}
          keyExtractor={(item,index) => index.toString()}
          contentContainerStyle={{}}
          horizontal
          showsHorizontalScrollIndicator={false}
          maxToRenderPerBatch={4}
          onEndReachedThreshold={0.5}
        />
      
    );
};
const MyCarouselportrait = StyleSheet.create({
    image: {
      width: wp('100%'),
      height: hp('25%'),
      borderRadius: 5,
    },
  })
  const MyCarousellandscape = StyleSheet.create({
    image: {
     ...MyCarouselportrait.image,
    },
  })

export default MyCarousel;



















{/* <View style={Style.body}>
<Text style={Style.text}>
    Screen A
</Text>
<Pressable
    onPress={()=>{}}
    style={({ pressed }) => ({ backgroundColor: pressed ? '#ddd' : '#0f0' })}
>
    <Text style={Style.text}>
        Go to Screen B
</Text>
</Pressable>
</View> */}