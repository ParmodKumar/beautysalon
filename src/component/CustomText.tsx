import * as React from 'react';
import {Text,StyleSheet, TextStyle} from 'react-native';
import {Typography} from '../style/Global'

interface Props{
    children:any;
    style?:TextStyle | TextStyle[];
    onPress?:any;
    numberOfLines?:any;
}

const CustomText = (props:Props)=>{
    return(
        <Text  numberOfLines={props.numberOfLines} allowFontScaling={false} style={[Styles.text,props.style,{margin:0,padding:0}]}>{props.children }</Text>
    )
}



export default CustomText;


const Styles = StyleSheet.create({
    text:{
        ...Typography.subheading
    }
})