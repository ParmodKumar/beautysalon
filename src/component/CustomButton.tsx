import * as React from 'react';
import { TouchableOpacity,Text, ViewStyle,TextStyle,StyleSheet, Pressable, View,Dimensions} from 'react-native';
import {widthPercentageToDP as wp,
    heightPercentageToDP as hp,
    listenOrientationChange as lor,
    removeOrientationListener as rol ,
    heightPercentageToDP,
    widthPercentageToDP} from 'react-native-responsive-screen';
import {PrimaryTheme} from '../style/Theme';
import { useOrientation } from '../utills/useOrientation';
import CustomText from './CustomText';
import Icon from './Icon';


    interface Props{
        onPress?:any,
        title : string,
        disabled? :boolean,
        buttons? : ViewStyle | ViewStyle[],
        textstyle? : TextStyle | TextStyle[],
        // useIcon?: boolean;
        // IconName?:string;
        // IconSize?:number;
        // IconColor?:string
        orientation?:string
        }
    const CustomButton = (props:Props) => {
        const orientation = useOrientation();

        return(
            <View style={orientation === 'portrait' ? 
            portraitbuttonstyle.ImageOverlaysec :  landscapetextstyle.ImageOverlaysec}>
                <TouchableOpacity  style={[orientation === 'portrait'
                ? portraitbuttonstyle.buttonstyle
                : landscapetextstyle.buttonstyle,props.buttons]}
                onPress={props.onPress}
                disabled={props.disabled}
                
                >  
                <Text   allowFontScaling={false} adjustsFontSizeToFit style={[
                    orientation === 'portrait'
                    ? portraitbuttonstyle.textstyle
                    : landscapetextstyle.textstyle,props.textstyle
                ]}>
                    {props.title}
                </Text> 
            </TouchableOpacity>
            </View>
          
            )
            }
        CustomButton.defaultProps = {
            disabled:false, 
            // useIcon: false,
            // IconSize:20,
            // IconColor:'red'
        }
        const portraitbuttonstyle = StyleSheet.create({
            buttonstyle:{    
                backgroundColor:'#fff',
                borderRadius: 6,
                width:wp('93%'),
                borderWidth:1,
                borderColor:'#fff',
                height:hp('6.9%'),
                alignItems:'center',
                flexDirection:'row',
                justifyContent:'center',
                // marginBottom:10
            },
            textstyle:{
                color:PrimaryTheme.$TEXT_COLOR,
                fontSize:wp("4.5%"), 
               
            },
            ImageOverlaysec:{ 
              },
        })
        const landscapetextstyle = StyleSheet.create({
            buttonstyle:{    
               ...portraitbuttonstyle.buttonstyle, width:wp('100%'),    
            },
            textstyle:{
                ...portraitbuttonstyle.textstyle,
            },
            ImageOverlaysec:{
              
              },
        })
export default CustomButton;


