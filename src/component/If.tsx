import * as React from 'react';
import {} from 'react-native';

interface Props{
    show:any;
    children:any
}

const If = (props:Props) =>{
    return(
        <>
        {props.show ? props.children : null}
        </>
    )
}


export default If;