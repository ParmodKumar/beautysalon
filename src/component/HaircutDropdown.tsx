import React, { useState, useEffect } from 'react';
// import CheckBox from '@react-native-community/checkbox';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { TouchableOpacity, View ,Text,StyleSheet} from 'react-native';
import { PrimaryTheme } from '../style/Theme';
import Icon from './Icon';
import { Typography } from '../style/Global';
import CustomText from './CustomText';

interface Props{
  Trim?:any;
  TrimM?:any;
  Rs?:any;
  Stylist?:any,
  Stylist1?:any;
  Rs1?:any
}
const HaircutDropdown = (props:Props)=>{
    const [toggleCheckBox, setToggleCheckBox] = useState(false)
      return(
            <>
             <View style={{borderBottomWidth:1,borderColor:PrimaryTheme.$Dim_Gray}}>
                          <View style = {Style.container1}>
                            <View style = {Style.redbox} >
                                <CustomText style={[Typography.lighText,{marginBottom:0} ]}>
                                {props.Trim}{'\n'}{props.TrimM}
                                </CustomText>
                               </View>
                                <View style = {Style.bluebox} >
                                  <Text style={{textAlign: 'right',}}>{props.Rs}</Text>
                                </View>
                                  <View style = {Style.blackbox} >
                                  {/* <CheckBox style={{transform:[{scaleX: 1.3}, {scaleY:1.3}] ,marginBottom:15,}}
                                    tintColors={{ true: PrimaryTheme.$BACKGROUND_COLOR, false: PrimaryTheme.$BACKGROUND_COLOR,}}
                                    disabled={false}
                                    value={toggleCheckBox}
                                    onValueChange={(newValue) => setToggleCheckBox(newValue)}
                                /> */}
                              </View>
                      </View>
                      <View style = {Style.container1}>
                            <View style={Style.redbox} >
                                <CustomText style={[Typography.lighText,{marginBottom:0}]}>
                                {props.Stylist}{'\n'}{props.Stylist1}
                                </CustomText>
                               </View>
                                <View style={Style.bluebox} >
                                  <Text style={{ textAlign: 'right', }}>{props.Rs1}</Text>
                                </View>
                                  <View style={Style.blackbox} >
                                 <View style={{}}>
                                 {/* <CheckBox style={{transform:[{scaleX: 1.3}, {scaleY:1.3}] ,marginBottom:15,}}
                                  tintColors={{ true: PrimaryTheme.$BACKGROUND_COLOR, false: PrimaryTheme.$BACKGROUND_COLOR,}}
                                  disabled={false}
                                  value={toggleCheckBox}
                                  onValueChange={(newValue) => setToggleCheckBox(newValue)}
                                /> */}
                                 </View>
                              </View>
                      </View>
                  </View>
        </>
    )
}


const Style  = StyleSheet.create({
  container1: {
    flexDirection: 'row',
    marginTop:10
  },
  redbox: {
    width: wp('34%'),
  },
  bluebox: {
    width: wp('32%'),
  },
  blackbox: {
  width: wp('20%'),
  alignItems: 'flex-end',
  },
  })
export default HaircutDropdown;


{/* <TouchableOpacity style={{borderWidth:2,width:wp('6.5%'),height:hp('4%'),
borderColor:PrimaryTheme.$BACKGROUND_COLOR,borderRadius:3,}}>
<Icon name={'add'} size={21} color={PrimaryTheme.$BACKGROUND_COLOR} style={{fontWeight: '900',}}/>
</TouchableOpacity> */}