import * as React from 'react';
import {Text,StyleSheet,View,TextInput, TouchableHighlight,Button} from 'react-native';
import Container from './Container';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import Icon from './Icon';
import { Sae } from 'react-native-textinput-effects';
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';
import { PrimaryTheme } from '../style/Theme';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { Spaning } from '../style/Global';

interface Props {
  placeholder?:any;
}
const Google = (props:Props)=>{
  
    return(
          <View style={styles.sectionStyle}>
            <TextInput 
              style={{}}
              placeholder={props.placeholder}
            />
            <Icon  name={'search-outline'} size={20} style={{marginTop: 10,}}
                  color={PrimaryTheme.$Dim_Gray}
            />  
          </View>
        
    )
}

export default Google;

const styles = StyleSheet.create({
  sectionStyle: {
    flexDirection: 'row',
    backgroundColor: '#fff',
    borderColor: '#000',
    borderRadius: 5,
    justifyContent:"space-between",
    marginBottom:Spaning.small.marginBottom
  },
  // GoogleText:{
  //   width:wp('90%')
  // }
});


