import { Text, View, Animated, StyleSheet, TouchableOpacity, ScrollView, LayoutAnimation,} from 'react-native';
import React, { useState, useEffect } from 'react';
import {List ,} from 'react-native-paper';
import { PrimaryTheme } from '../style/Theme';
import HaircutDropdown from './HaircutDropdown';

interface Props{
    drap1?:any
}
const AccordionGroup = (props) => {

    return(
       <View>
            <List.AccordionGroup>  
                <List.Accordion key={props.id} titleStyle={{}} title={<View style={{}}>
                    <Text style={{fontSize:14.5,fontWeight:"bold"}}>{props.drap1}</Text></View>} id="1" 
                  style={{borderColor:PrimaryTheme.$Dim_Gray,padding:0}}
                  theme={{ colors: { primary: '#000' ,}}}>
                    <List.Item 
                        titleStyle={{}} style={{padding:0}} key={props.index} 
                        title={
                         <View style={{borderTopWidth:1,}}>
                            <HaircutDropdown Trim="Trim" 
                                TrimM='10 m' 
                                Rs='₹ 200'
                                Stylist='Haircut - Stylist'
                                Stylist1='30 m'
                                Rs1='₹ 400'
                               />
                            </View>
                       } 
                    />
                </List.Accordion>
             
          </List.AccordionGroup>
        
       </View>
    )
}

export default AccordionGroup;