import React from 'react';
import { View ,Image,Text} from 'react-native';
import { Typography } from '../style/Global';
import CustomText from './CustomText';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import Icon from './Icon';
import { PrimaryTheme } from '../style/Theme';
import { Utils } from '../utills/utils';

interface Props{
    img?:any,
    title?:any,
    title1?:any,
    title2?:any,
    time?:any
}
const Saloncall = (props:Props)=>{
    return(
        <>
        <View style={{flexDirection:"row",marginTop:10}}>
           <View style={{width: wp('28%'),}}>
                 <Image style={{width:wp('24%'),height:hp('12%')}} resizeMode={'contain'} source={props.img} 
                />
           </View>
           <View style={{width: wp('40%'),}}>
           <CustomText style={[Typography.heading,{fontSize:wp('3.7%'),color:PrimaryTheme.$Black,marginBottom:5}]}>{props.title}</CustomText>
           <View style={{marginBottom:20}}>
           <CustomText style={[Typography.subheading,{fontSize:wp('3.5%'),color:PrimaryTheme.$Dim_Gray,}]}>{props.title1} 
           <CustomText style={[Typography.subheading,{fontSize:wp('3.5%'),color:PrimaryTheme.$Dim_Gray,}]}> {props.title2} </CustomText>
           </CustomText>
           </View>
           <CustomText style={[Typography.lighText,,{fontSize:14}]}>{props.time}</CustomText>
           </View>
           <View style={{width: wp('22%'),alignItems:"flex-end"}}>
             <View style={{flexDirection:"row"}}>
              <Icon name={'star'} size={14} style={{}}
                  color={PrimaryTheme.$BACKGROUND_COLOR}
              /> 
              <Text>  4.5</Text>
              </View>
               
              <View style={{marginTop:40,flexDirection:"row"}}>
              <Icon name={'call'} size={20} style={{marginRight:10,}}
                  color={PrimaryTheme.$BACKGROUND_COLOR}
              /> 
                <Icon name={'close'} size={20} style={{backgroundColor: PrimaryTheme.$BACKGROUND_COLOR,borderRadius: 10,}}
                  color='#FFF'
              /> 
               
              </View>
              
             </View>
         
         </View>
        </>
    )
}

export default Saloncall;