import React from 'react'
import { View, Text, StyleSheet, Dimensions, Image } from "react-native"
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import ImageOverlay from "react-native-image-overlay";
import CustomText from './CustomText';
import { Typography } from '../style/Global';

interface Props{
 
}
const CarouselCardItem = ({ item:props, index}) => {
  // const CarouselCardItem = (props:Props) => {
   

  return (
    <View style={{}} key={index}>
      <ImageOverlay 
         source={props.image}
          containerStyle={styles.image} 
          >
        <View>
          <CustomText style={[Typography.paragraph,{color:"#fff",textAlign:"center"}]}>Hair Styling</CustomText>
          <CustomText style={[Typography.paragraph,{color:"#fff",fontSize: 20,}]}>PRIDE TWIN SALON</CustomText>
          <CustomText style={[Typography.paragraph,{color:"#fff",textAlign:"center"}]}>20% OFF on Haircuts</CustomText>
         </View>
      </ImageOverlay>
    </View>
  )
}
const styles = StyleSheet.create({
  container: {
    // backgroundColor: '#fff',
    // borderRadius: 18,
    // width: wp('80%'),
    // height: hp('25%'),
  },
  image: {
    width: wp('80%'),
    height: hp('25%'),
    borderRadius: 18,
  },
  header: {
    color: '#fff',
    fontSize:10,
    fontWeight: "bold",
  },
})

export default CarouselCardItem;