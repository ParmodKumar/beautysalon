import * as React from 'react';
import { Text, View,Image,StyleSheet, TouchableOpacity } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import CustomButton from '../component/CustomButton';
import CustomText from '../component/CustomText';
import Icon from '../component/Icon';
import { Typography } from '../style/Global';
import { PrimaryTheme } from '../style/Theme';
import { Utils } from '../utills/utils';

const Favoriteslist = ()=>{
    return (
        <>
            <View style={{flexDirection:"row",justifyContent:"space-between",borderBottomWidth:0.5,paddingBottom:20,marginBottom:10}}>
                <View style={{flexDirection:"row"}}>
                    <Image style={{width:wp('20%'),height:hp('11%')}} 
                    resizeMode={'contain'} source={Utils.image.OrangeTheSalon} 
                    />
                <View style={{marginLeft:10}}>
                    <CustomText style={[Typography.heading,{fontSize:wp('3.7%'),color:PrimaryTheme.$Black,marginBottom:5}]}>Orange The Salon</CustomText>
                        <CustomText style={[Typography.subheading,{fontSize:wp('4.5%'),color:PrimaryTheme.$Dim_Gray,marginBottom:10}]}>
                            Haircut  
                        </CustomText>
                    <View style={{flexDirection:"row"}}>
                        <CustomText style={[Typography.lighText,,{fontSize:15,marginRight:10}]}>
                            From ₹ 200
                        </CustomText>
                        <Icon name={'star'} size={17} style={{marginRight:5,}}
                          color={PrimaryTheme.$BACKGROUND_COLOR}
                      />  
                      <Icon name={'star'} size={17} style={{marginRight:5,}}
                          color={PrimaryTheme.$BACKGROUND_COLOR}
                      />  
                      <Icon name={'star'} size={17} style={{marginRight:5,}}
                          color={PrimaryTheme.$BACKGROUND_COLOR}
                      />  
                      <Icon name={'star'} size={17} style={{marginRight:5,}}
                          color={PrimaryTheme.$BACKGROUND_COLOR}
                      />  
                      <Icon name={'star'} size={17} style={{marginRight:5,}}
                          color={PrimaryTheme.$Dim_Gray}
                      />  
                      <Text style={{marginLeft:10}}>58</Text>
                    </View>
                </View>
                </View>
 
                <View style={{justifyContent:"space-between",}}>
                  <View style={{marginTop:20}}>
                    <Icon name={'heart'} size={20} 
                        style={{backgroundColor: PrimaryTheme.$BACKGROUND_COLOR,borderRadius:10,
                            paddingTop: 2,paddingRight: 2,paddingLeft: 2,}}
                        color='#FFF'
                    /> 
                  </View>
                </View>
               
                 </View> 
                
        </>
    )
}
const Style = StyleSheet.create({
    ButtonTime:{
      backgroundColor:PrimaryTheme.$BACKGROUND_COLOR,
      width:wp('30%'),
      borderRadius: 10,
      height: hp('6.5%'),
      fontSize:14,
      padding:9,
      letterSpacing:1, 
      marginRight:10
  },
    })
export default Favoriteslist;


