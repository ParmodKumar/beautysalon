import * as React from 'react';
import { View ,StyleSheet, TouchableOpacity} from 'react-native'
import { Spaning, Typography } from '../style/Global';
import CustomText from './CustomText';
import { PrimaryTheme } from '../style/Theme';

interface Props{
    title1?:any;
    title2?:any;
    onPress?:any,
    Textcolor?:any
}

const Viewall = (props:Props)=>{
    return(
        <View style={style.Viewall}>
            <CustomText style={[Typography.title,{fontSize:18.9, fontWeight:'900',},props.Textcolor]}>
                {props.title1}
            </CustomText>
            <TouchableOpacity onPress={props.onPress}>
               <CustomText style={[Typography.title,{fontSize:17, fontWeight:'900',color:PrimaryTheme.$BACKGROUND_COLOR}]}>
                {props.title2}
               </CustomText>
            </TouchableOpacity>
        </View>
    )
} 

const style  = StyleSheet.create({
    Viewall:{
        flexDirection:"row",
        justifyContent:"space-between",
        marginBottom:Spaning.tiny.marginBottom
      },
})
export default Viewall;