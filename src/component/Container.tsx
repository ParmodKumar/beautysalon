import * as React from 'react';
import { SafeAreaView,StatusBar,StyleSheet, View, ViewStyle } from 'react-native'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

interface Props{
children : any;
ContainerStyle?:ViewStyle | ViewStyle[],
Singinput?: ViewStyle | ViewStyle[],
}
interface State{
    children:any;
}
const Container = (props:Props)=>{
    return(
        
        <SafeAreaView style={[style.container,props.ContainerStyle,props.Singinput]}>
            {props.children }
        </SafeAreaView>
        
    )
}

export default Container;
const style = StyleSheet.create({
    container:{
        flex:1,
        justifyContent:"center",
        alignItems:"stretch",
       
    }
})