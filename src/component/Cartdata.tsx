import React from 'react';
import { View ,Image,Text} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { Typography } from '../style/Global';
import { PrimaryTheme } from '../style/Theme';
import { Utils } from '../utills/utils';
import CustomText from './CustomText';
import Icon from './Icon';

interface Props{
  img?:any,
  title?:any;
  time?:any;
  num?:any,
  num1?:any
}
const Cartdata = (props:Props) => {
    return(
        <>
        <View style={{flexDirection:"row",marginBottom:30}}>
               <View style={{width:wp('20%')}}>
               <Image style={{width:wp('15%'),height:hp('10%')}} resizeMode={'contain'} source={props.img} />
               </View>
               <View style={{width:wp('49%')}}>
               <CustomText style={[Typography.lighText,]}>{props.title}</CustomText>
              <Text>{props.num} <Text style={{color:PrimaryTheme.$BACKGROUND_COLOR,fontWeight:"bold"}}>{props.num1}</Text></Text>
               </View>
               <View style={{width:wp('23%'),}}>
               <CustomText style={[Typography.lighText,]}>{props.time}
                          <Icon name={'chevron-down-outline'} size={20} style={{}}
                            color={PrimaryTheme.$BACKGROUND_COLOR}
                          />  
               </CustomText>
              <View style={{alignItems:"flex-end"}}>
              <Icon name={'trash-outline'} size={20} style={{}}
                            color={PrimaryTheme.$BACKGROUND_COLOR}
                          />
                </View>  
               </View>
           </View>
          
        </>
    )
}

export default Cartdata;

// Utils.image.scissor