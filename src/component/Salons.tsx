import * as React from 'react';
import {Text, View,Image,StyleSheet, ScrollView, TouchableOpacity} from 'react-native';
import Container from '../component/Container';
import CustomText from '../component/CustomText';
import { Spaning, Typography } from '../style/Global';
import { Utils } from '../utills/utils';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { PrimaryTheme } from '../style/Theme';
import Icon from '../component/Icon';
import { color } from 'react-native-reanimated';
import { useOrientation } from '../utills/useOrientation';

interface Props{
  onPress?:any,
  imges?:any,
  heading?:any,
  headingnum?:any,
  subheading?:any,
  subheadingkm?:any,
  subheadingnum?:any,
  Service?:any
}

const Solons = (props:Props) =>{
  const orientation = useOrientation();

    return(
      <View style={{  marginBottom:Spaning.tiny.marginBottom,}}>
      <TouchableOpacity onPress={props.onPress} activeOpacity={0.8}>
       <View style={{flexDirection:"row"}}>
       <Image 
            source={props.imges}
            style={orientation === 'portrait'  ? portraitSolons.HairServicesimg :  landscapeSolons.HairServicesimg}
        />
       </View>
      </TouchableOpacity>
      <View style={{flexDirection:"row",justifyContent:"space-between"}}>
          <CustomText style={[Typography.heading,orientation === 'portrait'  ? portraitSolons.Salonsheading :  landscapeSolons.Salonsheading]}>{props.heading}</CustomText>
          <View style={{flexDirection:"row"}}>
          <Icon name={'star'} size={16} style={{marginRight:5,}}
                  color={PrimaryTheme.$BACKGROUND_COLOR}
              />  
         <CustomText style={[Typography.heading,orientation === 'portrait'  ? portraitSolons.Salonsheading :  landscapeSolons.Salonsheading]}>{props.headingnum}</CustomText>
          </View>
  </View>
      <View style={{flexDirection:"row",marginBottom:2,justifyContent:"space-between"}}>
         <View style={{}}>
         <CustomText style={[Typography.subheading,orientation === 'portrait'  ? portraitSolons.Salonssubheading :  landscapeSolons.Salonssubheading]}>{props.subheading}</CustomText>
         </View>
         <CustomText style={[Typography.subheading,orientation === 'portrait'  ? portraitSolons.Salonssubheading :  landscapeSolons.Salonssubheading]}>{props.subheadingkm}</CustomText>
      </View>
      <CustomText style={[Typography.subheading,orientation === 'portrait'  ? portraitSolons.Salonssubheading :  landscapeSolons.Salonssubheading]}>₹ 
      <CustomText style={[Typography.subheading,orientation === 'portrait'  ? portraitSolons.Salonssubheading :  landscapeSolons.Salonssubheading,{color:PrimaryTheme.$BACKGROUND_COLOR}]}> {props.subheadingnum}</CustomText>{props.Service}</CustomText>
     </View>
    )
}

const portraitSolons = StyleSheet.create({
  HairServicesimg:{
      width: wp('100%'),
      height: hp('30%'),
      borderRadius: 5,
      marginBottom:Spaning.tiny.marginBottom,
      marginTop:4,
      flex:1,
  },
  Salonsheading:{
      color:PrimaryTheme.$Black,
      fontSize:wp('3.9%')
  },
  Salonssubheading:{
      color:PrimaryTheme.$Dim_Gray,
      fontSize:wp('3.7%'),
     
  }
})
const landscapeSolons = StyleSheet.create({
  HairServicesimg:{
    ...portraitSolons.HairServicesimg, height: hp('45%'),
  },
  Salonsheading:{
    ...portraitSolons.Salonsheading
  },
  Salonssubheading:{
    ...portraitSolons.Salonssubheading
  }
})
export default Solons;
