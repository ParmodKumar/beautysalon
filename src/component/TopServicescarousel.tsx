import React, { useState, useEffect } from 'react';
import { Text, View, Animated, StyleSheet, TouchableOpacity, FlatList, Image } from 'react-native';
import { Calendar } from 'react-native-calendars';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { images } from '../utills/Date';
import ImageOverlay from "react-native-image-overlay";
import { PrimaryTheme } from '../style/Theme';
import CustomText from '../component/CustomText';
import { landscapeTypography, Spaning, Typography } from '../style/Global';
import Container from '../component/Container';
import { useOrientation } from '../utills/useOrientation';

interface Props{
  onPress?:any;
  data?:any
}

const TopServicescarousel = (props) => {
  const orientation = useOrientation();

    const  renderItem = ({ item }) => (
        <View style={{}}>
            <TouchableOpacity activeOpacity={0.8}
                onPress={props.onPress} style={{marginRight:Spaning.tiny.marginRight}}>
                    <Image  
                      source={item.image}
                      style={[orientation === 'portrait' ?  TopServiceportrait.image 
                    :TopServicelandscape.image ]}
                    />
                <View style={{alignItems:'center',marginRight:Spaning.tiny.marginRight,margin:3}}>
                    <CustomText style={[ orientation === 'portrait' ? Typography.heading : 
                    landscapeTypography.heading,{textAlign:"center",color:PrimaryTheme.$Black,fontSize:wp('3.5%'),}]}>{item.subTitle}</CustomText>
                    <CustomText style={[ orientation === 'portrait' ? Typography.subheading : 
                    landscapeTypography.subheading,{textAlign:"center",color:PrimaryTheme.$Black,fontSize:wp('3.5%'),}]}>{item.subPoint}</CustomText>
                </View>
            </TouchableOpacity>      
        </View>
      );
    return (
        <FlatList showsVerticalScrollIndicator={false}
          data={props.data}
          renderItem={renderItem}
          keyExtractor={(item,index) => index.toString()}
          contentContainerStyle={{}}
          horizontal
          showsHorizontalScrollIndicator={false}
          maxToRenderPerBatch={5}
        />
    );
};

const TopServiceportrait = StyleSheet.create({
    image: {
        width:wp('22%'),
        height: hp('10%'),
        borderRadius: 5,
        justifyContent:"center"
      },
  })
  const TopServicelandscape = StyleSheet.create({
    image: {
       ...TopServiceportrait.image, width:wp('26%'),
      },
  })

export default TopServicescarousel;
