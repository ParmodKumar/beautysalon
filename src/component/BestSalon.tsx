import React, { useState, useEffect } from 'react';
import { Text, View, Animated, StyleSheet, TouchableOpacity, Pressable, FlatList, Image } from 'react-native';
import { Calendar } from 'react-native-calendars';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { images } from '../utills/Date';
import ImageOverlay from "react-native-image-overlay";
import { PrimaryTheme } from '../style/Theme';
import CustomText from '../component/CustomText';
import { landscapeTypography, Spaning, Typography } from '../style/Global';
import Container from '../component/Container';
import Icon from '../component/Icon';
import { useOrientation } from '../utills/useOrientation';


interface Props{
  onPress?:any
}

const BestSalon = (props) => {
  const orientation = useOrientation();

    const  renderItem = ({ item }) => (
          <View style={{}}>
             <TouchableOpacity activeOpacity={0.8}
                  onPress={props.onPress} style={{marginRight:Spaning.tiny.marginRight}}>
                   <Image 
                  source={item.image}
                  style={[orientation === 'portrait' ? BestSalonportrait.image :
                   BestSalonlandscape.image]}
                />
                <View style={{flexDirection:"row",marginTop:Spaning.tiny.marginTop}}>
                  <CustomText style={[orientation === 'portrait' ? Typography.heading : landscapeTypography.heading ,
                  {fontSize:wp('4%'),fontWeight:'900',color:PrimaryTheme.$Black,
                    marginRight:Spaning.regural.marginRight}]}>{item.subTitle}
                  </CustomText>
                    <View style={{flexDirection:"row",marginLeft: Spaning.tiny.marginLeft}}>
                      <Icon name={'star'} size={hp('2.4%')} style={{marginRight:2,}}
                          color={PrimaryTheme.$BACKGROUND_COLOR}
                      />  
                      <CustomText style={[orientation === 'portrait' ? Typography.subheading :
                         landscapeTypography.subheading,{fontSize:wp('4.3%'),
                        fontWeight:'900',color:PrimaryTheme.$BACKGROUND_COLOR}]}>{item.subTitright}
                      </CustomText>
                    </View>
                </View>
                  <CustomText  style={[orientation === 'portrait' ? Typography.subheading :
                         landscapeTypography.subheading,,{fontSize:wp('4.3%'),
                    fontWeight:'900',color:PrimaryTheme.$Dim_Gray}]}>{item.subheading}
                  </CustomText>
                      </TouchableOpacity>      
            </View>
      );

    return (
        <FlatList showsVerticalScrollIndicator={false}
          data={images[2]}
          renderItem={renderItem}
          keyExtractor={(item,index) => index.toString()}
          contentContainerStyle={{}}
          horizontal
          showsHorizontalScrollIndicator={false}
          maxToRenderPerBatch={2}
        />
      
    );
};

const BestSalonportrait  = StyleSheet.create({
    image: {
        width:wp('44.5%'),height: hp('15%'),
        borderRadius: 5,
      },
  })

  const BestSalonlandscape = StyleSheet.create({
    image: {
       ...BestSalonportrait.image, width:wp('46.9%')
      },
  })

export default BestSalon;






