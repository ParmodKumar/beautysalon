import * as React from 'react';
import {Text, TouchableOpacity, View,Image, ScrollView,StyleSheet} from 'react-native';
import BestSalon from '../component/BestSalon';
import Container from '../component/Container';
import CustomText from '../component/CustomText';
import Google from '../component/Google';
import { Spaning, Typography } from '../style/Global';
import { images } from '../utills/Date';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { PrimaryTheme } from '../style/Theme';
import ImageOverlay from "react-native-image-overlay";
import { Utils } from '../utills/utils';
import { ScreenNames } from './HairStyling';
import { useOrientation } from '../utills/useOrientation';
import Viewall from '../component/Viewall';

interface Props{
    route:any;
    navigation:any
}
const TrendingServices = (props:Props) =>{
  const orientation = useOrientation();

    const TrendingSalons = () =>{
        props.navigation.navigate(ScreenNames.HAIRCUT)
      }
    return(
        <Container ContainerStyle={{justifyContent:"flex-start",
            marginTop:Spaning.small.marginTop,marginHorizontal:15}}>
             <ScrollView showsVerticalScrollIndicator={false}>
             <Google  
                placeholder={'Search'}
              />
               <View style={{}}>
                 <CustomText style={[Typography.title,
                  {fontSize:18.9, fontWeight:'900',
                  marginBottom:Spaning.tiny.marginBottom}]}>
                    Trending Hair Services</CustomText>
              </View>
              <View style={orientation === 'portrait' ? 
                TrendingportraitStyle.Carousel : TrendinglandscapeStyle.Carousel}>
                    <BestSalon 
                      onPress={TrendingSalons}
                    />  
              </View>
                <Viewall 
                  title1='Top Services'
                  title2='View all'
                  onPress={()=> props.navigation.navigate(ScreenNames.ViewLIST)}
                />
           <View style={{}}>
           <CustomText style={[Typography.paragraph,{marginBottom:15,fontSize:16}]}>
             Choose from a list of variety os hair services just for you  
                        </CustomText>
           </View>
           <View style={{flexDirection:"row",marginBottom:10}}>
            <ImageOverlay containerStyle={orientation === 'portrait' ? 
                TrendingportraitStyle.HairServicesimg : TrendinglandscapeStyle.HairServicesimg}
              source={Utils.image.Serviceshairstyle} 
              title="Haircut"
              overlayAlpha={0.3}
              overlayColor={PrimaryTheme.$Dim_Gray}
              titleStyle={{ fontWeight: 'bold'}} 
              /> 
             <ImageOverlay containerStyle={orientation === 'portrait' ? 
                TrendingportraitStyle.HairServicesimg : TrendinglandscapeStyle.HairServicesimg}
                source={Utils.image.ServicesSpa} 
                title="Spa"
                overlayAlpha={0.3}
              overlayColor={PrimaryTheme.$Dim_Gray}
              titleStyle={{ fontWeight: 'bold' }} 

                /> 

            </View>
            <View style={{flexDirection:"row"}}>
                <ImageOverlay containerStyle={orientation === 'portrait' ? 
                TrendingportraitStyle.HairServicesimg : TrendinglandscapeStyle.HairServicesimg}
              source={Utils.image.ServicesColoring} 
              title="Coloring"
              titleStyle={{ fontWeight: 'bold' }} 
              overlayAlpha={0.3}
              overlayColor={PrimaryTheme.$Dim_Gray}
              /> 
             <ImageOverlay containerStyle={orientation === 'portrait' ? 
                TrendingportraitStyle.HairServicesimg : TrendinglandscapeStyle.HairServicesimg}
                source={Utils.image.ServicesStyling} 
                title="Styling"
                overlayAlpha={0.3}
                overlayColor={PrimaryTheme.$Dim_Gray}
                titleStyle={{ fontWeight: 'bold' }} 
              /> 
                </View>
           </ScrollView>
        </Container>
    )
}

const TrendingportraitStyle = StyleSheet.create({
  HairServicesimg:{
    width: wp('50%'),height: hp('20%'),flex:1,marginRight:10,borderRadius: 5,
  },
  Carousel:{
    marginBottom:Spaning.tiny.marginBottom
  },
})


const TrendinglandscapeStyle = StyleSheet.create({
  HairServicesimg:{
    width: wp('50%'),height: hp('25%'),flex:1,marginRight:10,borderRadius: 5,
  },
  Carousel:{
    marginBottom:Spaning.tiny.marginBottom
  },
})

export default TrendingServices;


















{/* <View style={orientation === 'portrait' ? TrendingportraitStyle.parent : TrendinglandscapeStyle.parent }>
<View style={orientation === 'portrait' ? 
  TrendingportraitStyle.child : TrendinglandscapeStyle.child } >
  <Image 
          source={Utils.image.Haircut}
        style={orientation === 'portrait' ? TrendingportraitStyle.HairServicesimg :
        TrendinglandscapeStyle.HairServicesimg}
  />
</View>
<View style={orientation === 'portrait' ? 
  TrendingportraitStyle.child : TrendinglandscapeStyle.child } >
<Image 
    source={Utils.image.Spa}
    style={orientation === 'portrait' ? TrendingportraitStyle.HairServicesimg :
    TrendinglandscapeStyle.HairServicesimg}
/>
</View>
<View style={orientation === 'portrait' ? 
  TrendingportraitStyle.child : TrendinglandscapeStyle.child } >
<Image 
  source={Utils.image.Spa}
  style={orientation === 'portrait' ? TrendingportraitStyle.HairServicesimg :
        TrendinglandscapeStyle.HairServicesimg}
  />
</View>
<View style={orientation === 'portrait' ? 
  TrendingportraitStyle.child : TrendinglandscapeStyle.child } >
<Image 
  source={Utils.image.Styling}
  style={orientation === 'portrait' ? TrendingportraitStyle.HairServicesimg :
  TrendinglandscapeStyle.HairServicesimg}
/>
</View>
</View> */}














{/* <View style={{flexDirection:'row',}}>
               <View style={{marginRight:10,marginBottom:10}}>
               <Image 
                        source={Utils.image.Haircut}
                       style={Style.HairServicesimg}
                        />
                   <Image 
                        source={Utils.image.Coloring}
                        style={Style.HairServicesimg}
                        />
               </View>
               <View style={{}}>
               <Image 
                        source={Utils.image.Spa}
                        style={Style.HairServicesimg}
                        />
                   <Image 
                        source={Utils.image.Styling}
                        style={Style.HairServicesimg}
                        />
               </View>
           </View> */}