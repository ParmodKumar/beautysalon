
import * as React from 'react';
import { Button, Text, TouchableOpacity, TouchableWithoutFeedback, View,TouchableHighlight } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Icon from '../component/Icon';
import { PrimaryTheme } from '../style/Theme';
import HomeScreen from './HomeScreen';
import CartScreen from './CartScreen';
import BookingScreen from './BookingScreen';
import FavoritesScreen from './FavoritesScreen';
import ProfileScreen from './ProfileScreen';
import Mapsreen from './Mapsreen';
import { color } from 'react-native-reanimated';
import Viewlist from './Viewlist';
import TrendingServices from './TrendingServices';
import Haircut from './Haircut';
import SalonDelail from './SalonDelail';
import BookAppointment from './BookAppointment';
import Payment from './Payment';

interface Props{
  route:any;
  navigation:any
}
 
export enum ScreenNames {
 HomeSCREEN ='HomeScreen',
 CARDSCREEN = 'CardScreen',
 BOOKINGSREEN= 'BookingScreen',
 FAVORITESSREEN = 'FavoritesScreen',
 MAPSREEN = 'Mapsreen',
 ViewLIST = 'Viewlist',
 TRENDINGSERVICES= 'TrendingServices',
 HAIRCUT ='Haircut',
 SALONDELAIL= 'SalonDelail',
 BOOkAppointment ="BookAppointment",
 PAYMENT ='Payment'
}

const HomeStack = createStackNavigator();
  function HomeStackScreen(props:Props) {
    return(
      <>
        <HomeStack.Navigator  screenOptions={{ 
          headerShown: false,
          }}
          >
          <HomeStack.Screen name="Home" component={HomeScreen} />
          <HomeStack.Screen name="Mapsreen" component={Mapsreen} />
          <HomeStack.Screen options={{ headerShown: true,}} name="Viewlist" component={Viewlist} />
          <HomeStack.Screen options={{
             headerShown: true,
             title: 'Hair',
             headerTintColor:"#fff",
             headerTitleStyle: { alignSelf: 'center' ,color:"#fff"},
             headerStyle:{
              elevation:0,
              shadowOpacity: 0,
              backgroundColor:PrimaryTheme.$BACKGROUND_COLOR,
            }
          }} name="TrendingServices" component={TrendingServices} />
           <HomeStack.Screen options={{ headerShown: true, title: 'Haircut',
           headerTitleStyle:{
             alignSelf: 'center',
            
           },
           headerRight:()=>(
            <View style={{flexDirection:"row",marginHorizontal:15,}}>
        <Icon name={'search-outline'} size={24} style={{marginRight:10}}
              color='#fff'
            />    
        <Icon name={'radio-outline'} size={24}
              color='#fff'
            />    
      </View> 
             
           )
           }} name="Haircut" component={Haircut} />
        <HomeStack.Screen options={{ headerShown: true, 
          }}  name="SalonDelail" component={SalonDelail} />
                    <HomeStack.Screen options={{headerShown: true,
                      headerTitleStyle:{
                        alignSelf: 'center',
                       
                      },
                      headerRight:()=>(
                    <Icon name={'cart-outline'} size={24} style={{marginRight:10}}
                          color={PrimaryTheme.$BACKGROUND_COLOR}
                        />    

                         
                       )
                    }} name="BookAppointment" component={BookAppointment} />

        </HomeStack.Navigator>
      </>
    );
  }
const CartStack = createStackNavigator();
  function CardStackScreen({navigation }){
    return (
      <CartStack.Navigator screenOptions={{
        headerLeft:()=>(
            <TouchableOpacity onPress={()=>navigation.goBack(ScreenNames.HomeSCREEN)}>
              <Icon name={'chevron-back-outline'} size={30}
                color={PrimaryTheme.$BACKGROUND_COLOR}
              />    
            </TouchableOpacity>  
          ),    
          }}
        >
        <CartStack.Screen options={{
           headerTitleStyle:{
            alignSelf: 'center',
           
          },
          headerRight:()=>(
            <Icon name={'add'} size={24} style={{fontWeight: 'bold'}}
                  color={PrimaryTheme.$BACKGROUND_COLOR}
          />       
        )
        }} name="Cart" component={CartScreen}  />
          <CartStack.Screen options={{ headerShown: true,}} name="Payment" component={Payment} />
        </CartStack.Navigator>
    );
}
const BookingStack = createStackNavigator();
  function BookingStackScreen(props:Props) {
    return (
      <BookingStack.Navigator screenOptions={{
      
        headerLeft:()=>(
            <TouchableOpacity onPress={()=>props.navigation.goBack(ScreenNames.CARDSCREEN)}>
              <Icon name={'chevron-back-outline'} size={30} 
              color={PrimaryTheme.$BACKGROUND_COLOR}
            />    
            </TouchableOpacity>  
          ),       
        }}
      >
        <BookingStack.Screen options={{
          //  headerRight:()=>(
          //     <Icon name={'cart-outline'} size={30} style={{marginRight: 15,}}
          //     color={PrimaryTheme.$BACKGROUND_COLOR}
          //   />    
          
          // ),  
        }} name="Booking" component={BookingScreen}  />
      </BookingStack.Navigator>
    );
}

const FavoritesStack = createStackNavigator();
  function FavoritesStackScreen(props:Props) {
    return (
      <FavoritesStack.Navigator screenOptions={{
        headerLeft:()=>(
            <TouchableOpacity onPress={()=>props.navigation.goBack(ScreenNames.BOOKINGSREEN)}
            >
              <Icon name={'chevron-back-outline'} size={30} 
              color={PrimaryTheme.$BACKGROUND_COLOR}
            />    
            </TouchableOpacity>  
          ),    
      }}>
        <FavoritesStack.Screen  name="Favorites" component={FavoritesScreen}  />
      </FavoritesStack.Navigator>
    );
}

const ProfileStack = createStackNavigator();
  function ProfileStackScreen(props:Props) {
    return (
      <ProfileStack.Navigator screenOptions={{
        headerLeft:()=>(
            <TouchableOpacity onPress={()=>props.navigation.goBack(ScreenNames.FAVORITESSREEN)}
            >
              <Icon name={'chevron-back-outline'} size={30} 
              color={PrimaryTheme.$BACKGROUND_COLOR}
            />    
            </TouchableOpacity>  
          ),    
      }}>
        <ProfileStack.Screen  name="ProfileScreen" component={ProfileScreen}  />
      </ProfileStack.Navigator>
    );
}
const Tab = createBottomTabNavigator();
  const  HairStyling =() =>{
    return (
      <>
        <Tab.Navigator  tabBarOptions={{
           activeTintColor: '#81B247',
           inactiveTintColor: 'black',
           keyboardHidesTabBar: true,
            style: {
            // marginBottom:8,
            },
            labelStyle:{
              fontSize: 11,
            }
          }}
          screenOptions={({ route }) => ({
            tabBarIcon: ({ focused, color, size }) => {
              if (route.name === 'Home') {
                return <Icon name={'home-outline'} size={size} color={PrimaryTheme.$Suva_Grey}   />;
              } else if (route.name === 'Card') {
                return <Icon name={'cart-outline'} size={size} color={PrimaryTheme.$Suva_Grey} />;
              }else if(route.name === 'Bookings'){
                return <Icon name={'book-outline'} size={size} color={PrimaryTheme.$Suva_Grey} />;
              }else if(route.name === 'Favorites'){
                return <Icon name={'heart-outline'} size={size} color={PrimaryTheme.$Suva_Grey} />;
              }else if(route.name === 'Profile'){
                return <Icon name={'person-outline'} size={size} color={PrimaryTheme.$Suva_Grey} />;
              }
            },
          })}
          >
          <Tab.Screen name="Home" component={HomeStackScreen} />
          <Tab.Screen name="Card" component={CardStackScreen}  />
          <Tab.Screen name="Bookings" component={BookingStackScreen}  />
          <Tab.Screen name="Favorites" component={FavoritesStackScreen}  />
          <Tab.Screen name="Profile" component={ProfileStackScreen}  />
        </Tab.Navigator>
      </>
    );
  }
  export default HairStyling;
  