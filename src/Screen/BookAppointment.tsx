import React, { useState, useEffect } from 'react';
import { Text, View, Animated, StyleSheet, TouchableOpacity, ScrollView, FlatList, ImageBackground,Image} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {Calendar, CalendarList, Agenda} from 'react-native-calendars';
import CalendarStrip from 'react-native-calendar-strip';
import moment from 'moment';
import { PrimaryTheme } from '../style/Theme';
import Container from '../component/Container';
import AutoScrolling from 'react-native-auto-scrolling';
import CustomButton from '../component/CustomButton';
import CustomText from '../component/CustomText';
import { Typography } from '../style/Global';
import {List ,} from 'react-native-paper';
import Icon from '../component/Icon';
import HaircutDropdown from '../component/HaircutDropdown';
import AccordionGroup from '../component/AccordionGroup';
import TopServicescarousel from '../component/TopServicescarousel';
import { images } from '../utills/Date';
import { Utils } from '../utills/utils';
import { useOrientation } from '../utills/useOrientation';

const BookAppointment = (props) => {
  const orientation = useOrientation();

  const [toggleCheckBox, setToggleCheckBox] = useState(false)


  let startDate = moment(); // today

    // Create a week's worth of custom date styles and marked dates.

    for (let i=0; i<7; i++) {
      let date = startDate.clone().add(i, 'days');
    }
  const  datesBlacklistFunc = date => {
      return date.isoWeekday() === 6; // disable Saturdays
    }
  
  const onDateSelected = date => {
    ({ formattedDate: date.format('YYYY-MM-DD')});
  }

  const theme = {
    colors: {
      text: "#000",
    }
  };

  
    return (
      <Container ContainerStyle={{justifyContent:"flex-start",marginTop:5,marginHorizontal:15}}>
                 <ScrollView showsVerticalScrollIndicator={false}>

       <View>
       <CalendarStrip
          scrollable
          daySelectionAnimation={{type: 'background', duration: 300, highlightColor: ''}}
          calendarAnimation={{type: 'sequence', duration: 30}}
          style={{height:hp('15%'), paddingBottom: 10,}}
          calendarHeaderStyle={{color: PrimaryTheme.$BACKGROUND_COLOR,fontSize:16,fontWeight:'800',letterSpacing:0.3}}
          useIsoWeekday={false}
          iconContainer={{flex: 0.1}}
          datesBlacklist={datesBlacklistFunc}
          onDateSelected={onDateSelected}
          dateNumberStyle={{fontSize:16}}
          disabledDateOpacity={0.9}
          dateNameStyle={{fontSize:10}}
          responsiveSizingOffset={6}
        />
       </View>
      
 
                    <View style={{flexDirection:"row",marginBottom:10}}>
                    <CustomButton title={'10:00 AM'}  buttons={Style.ButtonTime}                 
                    />
                    <View style={{flexDirection:"row",marginTop:10,marginRight:10,}}>
                    <CustomText style={[Typography.heading,{color:PrimaryTheme.$BACKGROUND_COLOR,marginRight:10,fontSize:14,fontWeight:"500"}]}>11:00 AM</CustomText>
                    <CustomText style={[Typography.heading,{color:PrimaryTheme.$Dim_Gray,marginRight:10,fontSize:14,fontWeight:"500"}]}>12 :00 PM</CustomText>
                    <CustomText style={[Typography.heading,{color:PrimaryTheme.$BACKGROUND_COLOR,fontSize:14,fontWeight:"500"}]}>01 :00 PM</CustomText>
                    </View>
                    </View>
                    <View
                        style={{
                            borderBottomColor: PrimaryTheme.$Dim_Gray,
                            borderBottomWidth: 1,
                            marginTop:10,
                            marginBottom:5
                        }}
                        />
                <CustomText style={[Typography.title,{fontWeight:'900',}]}>Services</CustomText>

                <View style={{flexDirection:"row",marginBottom:10}}>
            <CustomButton title={'Hair'}  buttons={Style.ButtonHair}                 
            />
            <View style={{flexDirection:"row",marginTop:10,marginRight:10,}}>
            <CustomText style={[Typography.heading,{color:PrimaryTheme.$BACKGROUND_COLOR,marginRight:15,fontSize:14,fontWeight:"500"}]}>Face</CustomText>
            <CustomText style={[Typography.heading,{color:PrimaryTheme.$BACKGROUND_COLOR,marginRight:15,fontSize:14,fontWeight:"500"}]}>Skin</CustomText>
            <CustomText style={[Typography.heading,{color:PrimaryTheme.$BACKGROUND_COLOR,fontSize:14,fontWeight:"500"}]}>Hands & Feet</CustomText>
            </View>
            </View>
            <AccordionGroup drap1='Haircut'
            />
            <AccordionGroup drap1='Hair Care'
            />
            <AccordionGroup drap1='Hair Color'
            />
            <AccordionGroup drap1='Hair Styling'
            />
            <CustomText style={[Typography.title,{fontWeight:'900',marginBottom:10,fontSize:19}]}>
                 Popular Stylists
            </CustomText>
            <View style={{flexDirection:"row",}}>
            <Image
              style={Style.PopularStylists}
              source={Utils.image.Popular1}
            />
            <Image
              style={Style.PopularStylists}
              source={Utils.image.Popular2}
            />
             <Image
                style={Style.PopularStylists}
                source={Utils.image.Popular3}
            />
             <Image
              style={Style.PopularStylists}
              source={Utils.image.Popular4}
            />
            </View>
              <View style={{marginTop:10}}>
                <CustomButton title={'Book Now'} onPress={()=>{}} 
                    buttons={Style.ButtonSingin}                        
                />
                  
              </View> 
      </ScrollView>
       </Container>
    );
};

const Style  = StyleSheet.create({
  ButtonTime:{
    backgroundColor:PrimaryTheme.$BACKGROUND_COLOR,
    width:wp('27%'),
    borderRadius: 10,
    height: hp('6%'),
    fontSize:14,
    padding:9,
    letterSpacing:1, 
    marginRight:10
},
ButtonHair:{
  backgroundColor:PrimaryTheme.$BACKGROUND_COLOR,
  width:wp('20%'),
  borderRadius: 10,
  height: hp('6%'),
  fontSize:14,
  padding:9,
  letterSpacing:1, 
  marginRight:10
},
ButtonSingin:{
  backgroundColor:PrimaryTheme.$BACKGROUND_COLOR,
  marginBottom: hp('1%'),
 color:PrimaryTheme.$TEXT_COLOR,
 borderRadius: 10,
},
PopularStylists:{
  width: wp('13%'),height: hp('12%'),flex:1,borderRadius: 5,marginRight:3.9,
  marginBottom:10
}
})
export default BookAppointment;
{/* <AutoScrolling style={{}} endPadding={50}>
<View style={{flexDirection:"row"}}>
<CustomButton title={'10:00 AM'}  buttons={Style.ButtonTime}                 
 />
 <Text style={{color:"#000",margin:10,marginRight:10}}>11:00 AM</Text>
 <Text style={{color:"#000",margin:10,marginRight:10}}>12 :00 PM</Text>
 <Text style={{color:"#000",margin:10,marginRight:10}}>01 :00 PM</Text>
</View>

</AutoScrolling> */}

{/* <List.AccordionGroup>
            
<List.Accordion descriptionStyle={{borderBottomWidth: 1,}}  title={
<CustomText style={[Typography.heading,
  {color:PrimaryTheme.$Black,fontSize:16.3,
    fontWeight:'100',}]}>Haircut
    
    </CustomText>
    }
id="1" 
style={{marginTop: 0,padding:0,}} theme={{ colors: { primary: '#000' }}}>
 <View style={{margin:0}}>
 <CustomText style={[Typography.heading,{color:PrimaryTheme.$Black,fontSize:10.3,fontWeight:'100',margin:0,padding:0}]}>HELLO</CustomText>
 </View>
 
</List.Accordion>
</List.AccordionGroup> */}

{/* <View style={{flexDirection:"row",backgroundColor:"red"}}>
                            <View style={{width: wp('49%'),}}>
                             <Text>HELLO</Text>
                            </View>
                            <View style={{width: wp('20%'),}}>
                            <Text>HELLO</Text>
                             
                            </View>
                            <View style={{width: wp('20%'),}}>
                            <Text>HELLO</Text>
                            </View>
                              </View> */}

                          //     <View style={{flexDirection:"row",marginBottom:10}}>
                          //     <Text style={{width:wp('62%')}}>Trim {'\n'}10 m</Text>
                          //     <Text style={{width:wp('20%')}}>₹ 200</Text>
                          //       <TouchableOpacity style={{borderWidth:2,width:wp('7%'),height:hp('4%'),borderColor:PrimaryTheme.$BACKGROUND_COLOR,borderRadius:3}}>
                          //         <Icon name={'add'} size={21} color={PrimaryTheme.$BACKGROUND_COLOR} style={{fontWeight: '900',}}/>
                          //       </TouchableOpacity>
                          // </View>
                          // <View style={{flexDirection:"row"}}>
                          //     <Text style={{width:wp('62%')}}>Haircut - Stylist{'\n'}30 m</Text>
                          //     <Text style={{width:wp('17%')}}>₹ 400</Text>
                          //     <View style={{width:wp('10%')}}>
                          //     <Checkbox 
                          //       status={checked ? 'checked' : 'unchecked'}
                          //       onPress={() => {
                          //         setChecked(!checked);
                          //       }}
                          //     />
                          //       </View>
                          //     </View>




                  //         <View style={{borderBottomWidth:1,borderColor:PrimaryTheme.$Dim_Gray}}>
                  //         <View style = {Style.container1}>
                  //           <View style = {Style.redbox} ><Text>Trim {'\n'}10 m</Text>
                  //              </View>
                  //               <View style = {Style.bluebox} ><Text style={{ textAlign: 'right', }}>₹ 200</Text>
                  //               </View>
                  //                 <View style = {Style.blackbox} >
                  //                     <TouchableOpacity style={{borderWidth:2,width:wp('6.5%'),height:hp('4%'),marginLeft:10,
                  //                        borderColor:PrimaryTheme.$BACKGROUND_COLOR,borderRadius:3,}}>
                  //                       <Icon name={'add'} size={21} color={PrimaryTheme.$BACKGROUND_COLOR} style={{fontWeight: '900',}}/>
                  //                   </TouchableOpacity>
                  //             </View>
                  //     </View>
                  //     <View style = {Style.container1}>
                  //           <View style = {Style.redbox} ><Text>Haircut - Stylist{'\n'}30 m</Text>
                  //           </View>
                  //           <View style = {Style.bluebox} ><Text style={{ textAlign: 'right', }}>₹ 400</Text>
                  //           </View>
                  //             <View style = {Style.blackbox} >
                  //               <CheckBox style={{ transform: [{scaleX: 1.3}, {scaleY:1.3}] ,}}
                  //                 tintColors={{ true: PrimaryTheme.$BACKGROUND_COLOR, false: PrimaryTheme.$BACKGROUND_COLOR,}}
                  //                 disabled={false}
                  //                 value={toggleCheckBox}
                  //                 onValueChange={(newValue) => setToggleCheckBox(newValue)}
                  //               />
                  //             </View>
                  //     </View>
                  // </View>
                


            //       <List.AccordionGroup>  
            //       <List.Accordion key={props.id} title={<View style={{}}><Text style={{}}>Haircut</Text></View>} id="1" 
            //         style={{borderColor:PrimaryTheme.$Dim_Gray,padding:0}}
            //         theme={{ colors: { primary: '#000' }}}>
            //           <List.Item titleStyle={{}} style={{padding:0}} key={props.index} 
            //             title={
            //                <View>
            //                 <HaircutDropdown Trim="Trim" 
            //                     TrimM='10 m' 
            //                     Rs='₹ 200'
            //                     Stylist='Haircut - Stylist'
            //                     Stylist1='30 m'
            //                     Rs1='₹ 400'
            //                 />
            //             </View>
            //              } 
            //           />
            //       </List.Accordion>
            // </List.AccordionGroup>