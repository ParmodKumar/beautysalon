import React, { useState } from "react";
import { View, StyleSheet, Image } from "react-native";
import RadioButtonRN from 'radio-buttons-react-native';
import { ScrollView } from "react-native-gesture-handler";
import { heightPercentageToDP } from "react-native-responsive-screen";
import {widthPercentageToDP as wp,heightPercentageToDP as hp} from 'react-native-responsive-screen'
import Container from "../component/Container";
import { Utils } from "../utills/utils";
import { PrimaryTheme } from "../style/Theme";
import CustomButton from "../component/CustomButton";

    interface Props{
        route:any
        navigation:any
    }
const Payment = (props:Props) => {

const data = [
{
label: 'Paytm',
},
{
label: 'Gpay'
},
{
label: 'Net Banking',
}
];


return (
    <Container ContainerStyle={{justifyContent:"flex-start",marginTop:5,marginHorizontal:15}}>
    <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.sectionStyle}>
            <View>
            <Image source={Utils.image.Paymentlogo}/>
                </View>
                <View style={{marginTop:heightPercentageToDP('15%')}}>
                <RadioButtonRN
                data={data}
                selectedBtn={(e) => console.log(e)}
                boxStyle={{height:35,borderWidth:0,backgroundColor:'#fff'}}
                circleSize={10}
                activeColor={PrimaryTheme.$BACKGROUND_COLOR}
                textColor={PrimaryTheme.$BACKGROUND_COLOR}
                initial={1}
                />
                </View>
                <CustomButton title={'Pay ₹ 600'} onPress={()=>{}} 
                    buttons={styles.buttonStyleC}                        
                />
        </View>
    </ScrollView>
</Container>
);
};

const styles = StyleSheet.create({
buttonStyleC:{
backgroundColor: PrimaryTheme.$BACKGROUND_COLOR,
borderColor:'#fff',
marginTop:hp('3%'),
borderRadius: 12,
},
sectionStyle: {
marginTop:hp('1%')
},
});


export default Payment;