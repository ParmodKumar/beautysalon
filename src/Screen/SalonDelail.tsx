import * as React from 'react';
import {ScrollView, Text, View,StyleSheet,Image, TextInput, TouchableOpacity} from 'react-native';
import Container from '../component/Container';
import CustomButton from '../component/CustomButton';
import CustomText from '../component/CustomText';
import Icon from '../component/Icon';
import { Spaning, Typography } from '../style/Global';
import { PrimaryTheme } from '../style/Theme';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { Utils } from '../utills/utils';
import BestSalon from '../component/BestSalon';
import { images } from '../utills/Date';
import { ScreenNames } from './HairStyling';
import { useOrientation } from '../utills/useOrientation';

interface Props{
    route:any;
  navigation:any
}

const SalonDelail = (props:Props) =>{
    const BookAppointmentClick = () =>{
        props.navigation.navigate(ScreenNames.BOOkAppointment)
      }

      const date = new Date().toLocaleDateString();
      const time = new Date().toLocaleTimeString();
      const orientation = useOrientation();

    
    return(
        <Container ContainerStyle={{justifyContent:"flex-start",marginHorizontal:15,}}>
            <ScrollView showsVerticalScrollIndicator={false}>
                <CustomText style={[Typography.title,{fontWeight:'900',}]}>Orange The Salon</CustomText>
                    <CustomText style={[Typography.lighText,]}>Hair Salon</CustomText>
                        <View style={{flexDirection:"row",}}>
                         <Icon name={'star'} size={17} style={{marginRight:3,}}
                          color={PrimaryTheme.$BACKGROUND_COLOR}
                      />  
                      <Icon name={'star'} size={17} style={{marginRight:3,}}
                          color={PrimaryTheme.$BACKGROUND_COLOR}
                      />  
                      <Icon name={'star'} size={17} style={{marginRight:3,}}
                          color={PrimaryTheme.$BACKGROUND_COLOR}
                      />  
                      <Icon name={'star'} size={17} style={{marginRight:3,}}
                          color={PrimaryTheme.$BACKGROUND_COLOR}
                      />  
                      <Icon name={'star'} size={17} style={{marginRight:5,}}
                          color={PrimaryTheme.$Dim_Gray}
                      />  
                     
                        <CustomText style={[Typography.subheading,{color:PrimaryTheme.$Black,fontSize:14.3,marginLeft:5,}]}>4.0</CustomText>
                        <CustomText style={[Typography.subheading,{color:PrimaryTheme.$Black,fontSize:14.3,marginLeft:10}]}>(282 ratings)</CustomText>
                </View>
                <View style={{flexDirection:"row",marginTop:5}}>
                <CustomButton title={'About'}  buttons={orientation === 'portrait' ? 
                  SalonDelailportrait.ButtonAbout :  SalonDelaillandscape.ButtonAbout}
                             
                              />
                               <CustomText style={[Typography.subheading,{color:PrimaryTheme.$BACKGROUND_COLOR,fontSize:15.3,marginTop:9,marginRight:5,marginLeft:5}]}>Services</CustomText>
                               <CustomText style={[Typography.subheading,{color:PrimaryTheme.$BACKGROUND_COLOR,fontSize:15.3,marginTop:9,marginRight:5,marginLeft:5,}]}>Stylists</CustomText>

                </View>
                <CustomText style={[Typography.title,{fontWeight:'900',}]}>Gallery</CustomText>
                <View style={{flexDirection:"row",  
                   }}>
                 <View style={orientation === 'portrait' ? 
                  SalonDelailportrait.Gallerysec :  SalonDelaillandscape.Gallerysec}>
                 <Image style={orientation === 'portrait' ? 
                  SalonDelailportrait.Galleryimg :  SalonDelaillandscape.Galleryimg} source={Utils.image.Gallery1} /> 
                 <Image style={orientation === 'portrait' ? 
                  SalonDelailportrait.Galleryimg :  SalonDelaillandscape.Galleryimg} source={Utils.image.Gallery2} />
                                      </View>
                 <View style={orientation === 'portrait' ? 
                  SalonDelailportrait.GalleryCenter :  SalonDelaillandscape.GalleryCenter}>
                 <Image style={orientation === 'portrait' ? 
                  SalonDelailportrait.GalleryCenterimg :  SalonDelaillandscape.GalleryCenterimg} source={Utils.image.Gallery5} /> 
                 </View>
                 <View style={orientation === 'portrait' ? 
                  SalonDelailportrait.Gallerysec1 :  SalonDelaillandscape.Gallerysec1}>
                 <Image style={orientation === 'portrait' ? 
                  SalonDelailportrait.Galleryimg :  SalonDelaillandscape.Galleryimg} source={Utils.image.Gallery3} /> 
                 <Image style={orientation === 'portrait' ? 
                  SalonDelailportrait.Galleryimg :  SalonDelaillandscape.Galleryimg} source={Utils.image.Gallery4} />
                 </View>
                </View>
                <CustomText style={[Typography.title,{fontWeight:'900',}]}>Description</CustomText>
                <CustomText style={[Typography.paragraph,{color:PrimaryTheme.$Dim_Gray,fontSize:13,marginBottom:10}]}>
                    It has survived not only five centuries, but also the 
                    leap {"\n"}into electronic typesetting, remaining essentially {"\n"} unchanged.
                      </CustomText>
                      <View
                        style={{
                            borderBottomColor: PrimaryTheme.$Dim_Gray,
                            borderBottomWidth: 1,
                            marginTop:10,
                            marginBottom:5
                        }}
                        />
                         <CustomText style={[Typography.title,{fontWeight:'900',}]}>Information</CustomText>
                         <View style={[orientation === 'portrait' ? 
                              SalonDelailportrait.parent :  SalonDelaillandscape.parent]}>
                            <View style={[orientation === 'portrait' ? 
                               SalonDelailportrait.child :  SalonDelaillandscape.child]} >
                            <CustomText style={[Typography.lighText,]}>Monday - Thursday</CustomText>
                            <CustomText style={[Typography.lighText,]}>Friday - Sunday</CustomText>
                            </View>
                            <View style={[orientation === 'portrait' ? 
                               SalonDelailportrait.child :  SalonDelaillandscape.child]} >
                            <CustomText style={[Typography.lighText,]}>{time} PM</CustomText>
                            <CustomText style={[Typography.lighText,]}>01 : 00 PM - 11 : 00 PM</CustomText>
                            </View>
                          
                        </View>
                        <View style={{flexDirection:"row",justifyContent:"space-between"}}>
                 <CustomText style={[Typography.lighText,{fontSize:13.1, marginBottom:10,
                    }]}>
                    F-10, SN Shivalik Arcade, Near Prahladnagar{"\n"} 
                    Garden, Prahladnagar{"\n"}1.0 km
                </CustomText>
               <Icon  name={'navigate'} size={23}   style={{
                    alignSelf: 'center',
                    color: '#fff',
                    backgroundColor: PrimaryTheme.$BACKGROUND_COLOR,
                    paddingHorizontal: 9,
                    paddingVertical: 7,
                    borderRadius: 40,
                   
                  }}
                      color={PrimaryTheme.$BACKGROUND_COLOR}
                    /> 
             </View>
             <View style={{flexDirection:"row",justifyContent:"space-between", 
                           }}>
            <View style={{paddingTop: 10,}}>
            <CustomText style={[Typography.lighText,{fontSize:15}
                    ]}>
                   +12 3456 7890
                </CustomText>
            </View>
                <Icon  name={'call'} size={23}   style={{ marginBottom:Spaning.tiny.marginBottom,
                    alignSelf: 'center',
                    color: '#fff',
                    backgroundColor: PrimaryTheme.$BACKGROUND_COLOR,
                    paddingHorizontal: 9,
                    paddingVertical: 7,
                    borderRadius: 40,
                    marginRight: 10,
                  }}
                      color={PrimaryTheme.$BACKGROUND_COLOR}
                    /> 
               </View>
               <View
                        style={{
                            borderBottomColor: PrimaryTheme.$Dim_Gray,
                            borderBottomWidth: 1,
                            marginTop:10,
                            marginBottom:5
                        }}
                        />
           
                <View style={{flexDirection:"row",justifyContent:"space-between",marginBottom:10,}}>
                    <CustomText style={[Typography.title,{fontSize:18.9, fontWeight:'900',}]}>
                     Similar Salons Nearby</CustomText>
                 
                <CustomText style={[Typography.title,{fontSize:17, fontWeight:'900',color:PrimaryTheme.$BACKGROUND_COLOR}]}>View all</CustomText>
               
             </View>
            <View style={{marginBottom:10}}>
            <BestSalon 
                      sliderWidth={wp('100%')}
                      sliderHeight={hp('14%')}
                      itemWidth={wp('48%')}
                      SliderImage={{}}
                      autoplayInterval={4000}
                      data={images[3]} 
                     
                    />  
            </View>
<View style={{}}>
   <CustomButton title={'Book Appointment'} onPress={BookAppointmentClick} 
    buttons={orientation === 'portrait' ? 
    SalonDelailportrait.ButtonSingin :  SalonDelaillandscape.ButtonSingin}                        
   />
</View>
                      
         </ScrollView>
            </Container>
    )
}


export default SalonDelail;


// {"\n"}

const SalonDelailportrait = StyleSheet.create({
    ButtonAbout:{
        backgroundColor:PrimaryTheme.$BACKGROUND_COLOR,
        color:PrimaryTheme.$TEXT_COLOR,
        width:wp('38%'),
        borderRadius: 10,
        height: hp('6.5%'),
        fontSize:15.3,
        padding:9,
        letterSpacing:1,
    },
    parent: {
        width: '100%', 
        flexDirection: 'row', 
        flexWrap: 'wrap'
    },
    child: {
        width: '48%', 
        margin: '1%', 
    },
    Galleryimg:{
        marginBottom:10,
        width: wp('21%'),
        height:hp('14%'),
        borderRadius:10,
        flex:1
    },
    Gallerysec:{
        width:wp('22%'),marginRight:10,
    },
    Gallerysec1:{
        width:wp('22%'),marginLeft:10,
    },
    GalleryCenter:{
        width:wp('42%'),
    },
    GalleryCenterimg:{
        width: wp('41%'),borderRadius:10,height:hp('29.5%')
    },
    ButtonSingin:{
        backgroundColor:PrimaryTheme.$BACKGROUND_COLOR,
        marginBottom: hp('1%'),
       color:PrimaryTheme.$TEXT_COLOR,
       borderRadius: 10,
      },
  
})
const SalonDelaillandscape = StyleSheet.create({
    ButtonAbout:{
      ...SalonDelailportrait.ButtonAbout,
    },
    parent: {
        ...SalonDelailportrait.parent,
    },
    child: {
        ...SalonDelailportrait.child,
    },
    Galleryimg:{
        ...SalonDelailportrait.Galleryimg,
        width: wp('50%'),
        height:hp('20%'),
    },
    Gallerysec:{
        ...SalonDelailportrait.Gallerysec, 
        width:wp('50%'),
    },
    Gallerysec1:{
        ...SalonDelailportrait.Gallerysec1, 
        width:wp('50%'),
    },
    GalleryCenter:{
        ...SalonDelailportrait.GalleryCenter, 
        width:wp('60%'),
    },
    GalleryCenterimg:{
        ...SalonDelailportrait.GalleryCenterimg, 
        width: wp('60%'),height:hp('41.6%')
    },
    ButtonSingin:{
        ...SalonDelailportrait.ButtonSingin, 
      },
  
})

// SectionStyle: {
//     flexDirection: 'row',
//     justifyContent: 'center',
//     alignItems: 'center',
//     backgroundColor: '#fff',
//     borderColor: PrimaryTheme.$Dim_Gray,
//     height: 55,
//     borderBottomWidth:0.6,
//     marginBottom:10
// },