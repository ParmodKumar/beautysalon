import * as React from 'react';
import {Text,StyleSheet,View,TouchableOpacity,StatusBar,ActivityIndicator} from 'react-native';
import Container from '../component/Container';
import CustomText from '../component/CustomText';
import Icon from '../component/Icon';
import { Spaning, Typography } from '../style/Global';
import { PrimaryTheme } from '../style/Theme';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { ScreenNames } from './HairStyling';
import Google from '../component/Google'
import Carousel from 'react-native-snap-carousel';
import MyCarousel from '../component/MyCarousel';
import { ScrollView, TextInput } from 'react-native-gesture-handler';
import CarouselCardItem from '../component/CarouselCardItem'
import { images} from '../utills/Date';
import Viewall from '../component/Viewall';
import TopServicescarousel from '../component/TopServicescarousel';
import SliderB from '../component/TopServicescarousel';
import BestSalon from '../component/BestSalon';
import LottieView from 'lottie-react-native';

interface Props{
  route:any;
  navigation:any
}
const HomeScreen = (props:Props) =>{
  const [loading, setLoading] = React.useState(true);
    const mapClick = () =>{
    props.navigation.navigate(ScreenNames.MAPSREEN)
  }
  setTimeout(() => {
    setLoading(false);
  }, 100);
  const TopServicesclick = () =>{
    props.navigation.navigate(ScreenNames.TRENDINGSERVICES)
}
    return(
        <Container 
          ContainerStyle={{justifyContent:"flex-start",marginHorizontal:15,marginTop:Spaning.tiny.marginTop}}>
          {loading ? (
            <View style={{  flex: 1, justifyContent: "center", alignItems:"center",
            }}>
               {/* <ActivityIndicator size="large" color="red" style={{}} /> */}
               <LottieView 
                  style={{
                  height: hp('30%'),
                  }} 
                  source={require('../assets/53237-hair-replacement')} autoPlay loop />
            </View>
          ):(
          <ScrollView showsVerticalScrollIndicator={false} minimumZoomScale={8}>
            <StatusBar  backgroundColor={PrimaryTheme.$BACKGROUND_COLOR}/>
              <View style={{}}>
                  <View style={styles.headermap}>
                    <View style={{flexDirection:"row",}}>
                      <TouchableOpacity onPress={mapClick}>
                          <Icon name={'location-sharp'} size={25} style={{marginRight:Spaning.tiny.marginRight}}
                          color={PrimaryTheme.$BACKGROUND_COLOR}
                        />  
                      </TouchableOpacity>  
                        <CustomText style={[Typography.heading,{fontSize:16,color:PrimaryTheme.$Black}]}>High Rd, University</CustomText>  
                          <Icon name={'chevron-forward-outline'} size={20} style={{}}
                            color={PrimaryTheme.$Black}
                          />  
                    </View>
                  <View>
                    <Icon  name={'notifications-outline'} size={26}
                        color={PrimaryTheme.$BACKGROUND_COLOR}
                      />  
                  </View>   
              </View>    
              <Google  
              placeholder={'Search for salons, services..'}
              />
              <View>
                <View style={styles.Carousel}>
                  <MyCarousel 
                    onPress={TopServicesclick}
                  />
                </View>
                <Viewall 
                  title1='Top Services'
                  title2='View all'
                  onPress={()=> props.navigation.navigate(ScreenNames.ViewLIST)}
                />
                <View style={styles.Carousel}>
                  <TopServicescarousel 
                      onPress={TopServicesclick}
                       data={images[1]}
                    /> 
                </View>
                <Viewall title1='Best Salon'
                  title2='View all'
                  onPress={()=> props.navigation.navigate(ScreenNames.ViewLIST)}
                />
                <View style={styles.Carousel}>
                  <BestSalon 
                  onPress={TopServicesclick}
                  /> 
               </View>
               <Viewall 
                title1='Popular Stylists'
                title2='View all'
                onPress={()=> props.navigation.navigate(ScreenNames.ViewLIST)}
                />
                <View style={styles.Carousel}>
                  <BestSalon 
                    onPress={TopServicesclick}
                  />    
                </View>  
              </View>
           </View>
          </ScrollView>
            )}
        </Container>
    )
}
const styles = StyleSheet.create({
  Carousel:{
    marginBottom:Spaning.tiny.marginBottom
  },
  headermap:{
    flexDirection:"row",paddingBottom:5,
    justifyContent:"space-between",marginBottom:Spaning.tiny.marginBottom
  },
   });
export default HomeScreen;



