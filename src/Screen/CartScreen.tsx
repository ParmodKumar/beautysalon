import * as React from 'react';
import {ImageBackground, Text, View,StyleSheet,Image, Switch, TextInput} from 'react-native';
import { Utils } from '../utills/utils';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import Container from '../component/Container';
import CustomText from '../component/CustomText';
import { Spaning, Typography } from '../style/Global';
import Icon from '../component/Icon';
import { PrimaryTheme } from '../style/Theme';
import Cartdata from '../component/Cartdata';
import Viewall from '../component/Viewall';
import CustomButton from '../component/CustomButton';
import { ScrollView } from 'react-native-gesture-handler';
import { ScreenNames } from './HairStyling';

interface Props{
  route:any;
  navigation:any
}
const CartScreen = (props:Props) =>{
  const Payment=() =>{
    props.navigation.navigate(ScreenNames.PAYMENT)
}
    return(
      <Container ContainerStyle={{justifyContent:"flex-start",marginTop:5,marginHorizontal:15}}>
        <ScrollView showsVerticalScrollIndicator={false}>
          <View style={{marginBottom:10}}>
          <CustomText style={[Typography.title,{fontWeight:'900',}]}>
            Orange The Salon
          </CustomText>
          </View>
         <View style={{borderBottomWidth:1,marginBottom:20}}>
            <Cartdata 
              title='Haircut - Stylist' 
              time='10:00AM'
              num='₹'
              num1='400'
              img={Utils.image.scissor}
            />
         </View>
                <View style={{borderBottomWidth:1,marginBottom:20}}>
                    <Cartdata 
                      title={'Hair Spa -\nShoulder Length' }
                      time='10:00AM'
                      num='₹'
                      num1='400'
                    img={Utils.image.Wax}/>
                </View>
              <View style={Style.sectionStyle}>
              <TextInput 
              style={{}}
              placeholder={'Enter Coupon code'}
            />
          <View style={{paddingTop:10}}>
          <Text style={{color:PrimaryTheme.$BACKGROUND_COLOR,fontWeight:"bold"}}>Apply</Text>
          </View>
              </View>
          <View style={{borderBottomWidth:1,marginTop:100,marginBottom:10}}>
          </View>
                <Viewall  Textcolor={Style.Textcolor}
                  title1='Total Amount'
                  title2='₹ 1200'
                />
                <CustomButton title={'Checkout'} onPress={Payment} 
                    buttons={Style.ButtonSingin}                        
                />
                </ScrollView>
      </Container>
   
    )
}


export default CartScreen;


const Style = StyleSheet.create({
  ButtonSingin:{
    backgroundColor:PrimaryTheme.$BACKGROUND_COLOR,
    marginBottom: hp('1%'),
   color:PrimaryTheme.$TEXT_COLOR,
   borderRadius: 12,
  },
  sectionStyle: {
    flexDirection: 'row',
    backgroundColor: '#fff',
    borderColor: '#000',
    borderRadius: 5,
    justifyContent:"space-between",
    marginBottom:Spaning.small.marginBottom
  },
  Textcolor:{
    color:PrimaryTheme.$BACKGROUND_COLOR,
    fontWeight:"bold"
  }
});

{/* <Image style={{width:wp('5%'),height:hp('10%')}} source={Utils.image.scissor} />  */}