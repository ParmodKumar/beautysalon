import * as React from 'react';
import {Text,TextInput,StyleSheet,Image, View, TouchableOpacity,  SafeAreaView, StatusBar,ScrollView,
    Linking} from 'react-native';
import Container from '../component/Container';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import CustomText from '../component/CustomText';
import { landscapeTypography, Spaning, Typography } from '../style/Global';
import { Utils } from '../utills/utils';
import PhoneInput from 'react-native-phone-number-input';
import CustomButton from '../component/CustomButton';
import { PrimaryTheme } from '../style/Theme';
import { Colors } from "react-native/Libraries/NewAppScreen";
import { ScreenName } from '../utills/Navigations/Routes';
import { useOrientation } from '../utills/useOrientation';

interface Props{
   placeholder?: string;
   route:any;
   navigation:any
}

const SingIn = (props:Props) =>{
   const VerifyClick = () =>{
      props.navigation.navigate(ScreenName.VERIFTYACCOUNT)
  }
const [value, setValue] = React.useState("");
const [valid, setValid] = React.useState(false);
const [showMessage, setShowMessage] = React.useState(false);
const phoneInput = React.useRef<PhoneInput>(null);
 
const orientation = useOrientation();

   return(
      <Container ContainerStyle={{}}> 
        <ScrollView showsVerticalScrollIndicator={false} 
        contentContainerStyle={{justifyContent:"flex-start",marginHorizontal:10,}}>
            <View style={{alignItems: 'center',}}>
            <View>
            <CustomText style={[Typography.title,
               {marginTop:Spaning.small.marginTop,
               marginBottom:Spaning.small.marginBottom,}]}>Sign In
               </CustomText>     
               </View>   
               <Image resizeMode={'contain'} 
               style={orientation === 'portrait' ? Singportrait.SingInimg :Singlandscape.SingInimg} 
                source={Utils.image.SingiNimg} /> 
                     <View style={{}}>
                        <CustomText style={[Typography.title,{}]}>
                           Welcome to Beauty Salon
                        </CustomText>
                        <CustomText style={[orientation === 'portrait' ? 
                  Typography.paragraph : landscapeTypography.paragraph,{marginBottom:Spaning.small.marginBottom}]}>
                           Enter your mobile number to get One Time Password.
                        </CustomText>
                           {/* <StatusBar barStyle="dark-content" /> */}
                              <View style={{marginBottom:Spaning.regural.marginBottom}}>
                                
                                    <PhoneInput  placeholder={'Enter phone number'}
                                        containerStyle={orientation === 'portrait' ? 
                                        Singportrait.moblieinput : Singlandscape.moblieinput}
                                       textInputStyle={{margin: 0,height: hp('4%'),padding:0,}}
                                       ref={phoneInput}
                                       defaultValue={value}
                                       defaultCode="IN"
                                       onChangeFormattedText={(text) => {
                                          setValue(text);
                                       }}
                                       withDarkTheme
                                       autoFocus
                                     
                                    />
                                    <TouchableOpacity
                                       onPress={() => {
                                       const checkValid = phoneInput.current?.isValidNumber(value);
                                       setShowMessage(true);
                                       setValid(checkValid ? checkValid : false);
                                       }}
                                        >
                                    </TouchableOpacity>
                               
                              </View>
                              <CustomButton title={'Get OTP'}  
                              buttons={orientation === 'portrait' ? Singportrait.ButtonSingin : 
                              Singlandscape.ButtonSingin}
                              onPress={VerifyClick}
                              />
                              <TouchableOpacity
                                  onPress={() => Linking.openURL('http://google.com')} 
                                 >
                                 <CustomText style={orientation === 'portrait' ? Singportrait.GoogleButton : 
                              Singlandscape.GoogleButton
                                    }>Connect with 
                                       <CustomText style={{textAlign:"center",
                                         color:PrimaryTheme.$Suva_Grey, fontSize:hp('2.4%'),fontWeight:"bold",fontFamily:'Long'}}> Google 
                                       </CustomText>
                                 </CustomText>
                              </TouchableOpacity> 
                              
                        </View>
                        </View>
            </ScrollView>
      </Container>
   )
} 

export default SingIn


const Singportrait  = StyleSheet.create({
    SingInimg:{
    width: wp('100%'),
    height: hp('30%'),
    alignItems:'center',
    borderColor:"red",
    borderRadius: 10,
    marginBottom:Spaning.small.marginBottom,
    justifyContent:"center"
   },
moblieinput:{
width:wp('93%')
},
ButtonSingin:{
   backgroundColor:PrimaryTheme.$BACKGROUND_COLOR,
   marginBottom:Spaning.tiny.marginBottom,
   color:PrimaryTheme.$TEXT_COLOR,
   borderRadius: 10,
 },
 GoogleButton:{
   height:hp('7%'),
   backgroundColor:PrimaryTheme.$TEXT_COLOR,
   fontWeight:"600",
   borderRadius: 10,
   borderColor:PrimaryTheme.$light_silver,
   fontSize:hp('2.6%'),
   color:PrimaryTheme.$Suva_Grey,
   width:wp('93%'),
   textAlign:"center",
   paddingTop:10,
 },
})

const Singlandscape  = StyleSheet.create({
   SingInimg:{
  ...Singportrait.SingInimg,
},
moblieinput:{
   width:wp('100%')
},
ButtonSingin:{
   ...Singportrait.ButtonSingin,
   backgroundColor:'red',
},
GoogleButton:{
   ...Singportrait.GoogleButton,width:wp('100%'),
},
})
