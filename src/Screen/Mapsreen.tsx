import * as React from 'react';
import {Text,StyleSheet,View} from 'react-native';
import Container from '../component/Container';
import MapView  from 'react-native-maps';
import CustomButton from '../component/CustomButton';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

const Mapsreen = () =>{
    
    return(
        <Container>
          <View style={styles.container}>
              <MapView 
                style={styles.map}
                  initialRegion={{    
                latitude: 37.78825,
                longitude: -122.4324,
                latitudeDelta: 0.0922,
                longitudeDelta: 0.0421,
              }}
            />
         </View>
        </Container>
    )
}

const styles = StyleSheet.create({
    container: {
      ...StyleSheet.absoluteFillObject,
      height: hp('100%'),
      width: 400,
      justifyContent: 'flex-end',
      alignItems: 'center',
     
    },
    map: {
      ...StyleSheet.absoluteFillObject,
     
    },
   });
export default Mapsreen;

