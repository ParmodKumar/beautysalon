import * as React from 'react';
import { Text, View,Image,StyleSheet, TouchableOpacity ,ScrollView} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import Container from '../component/Container';
import CustomButton from '../component/CustomButton';
import CustomText from '../component/CustomText';
import Favoriteslist from '../component/Favoriteslist';
import Icon from '../component/Icon';
import { Typography } from '../style/Global';
import { PrimaryTheme } from '../style/Theme';
import { Utils } from '../utills/utils';

const FavoritesScreen = ()=>{
    return (
        <Container ContainerStyle={{justifyContent:"flex-start",marginTop:5,marginHorizontal:15}}>
            <ScrollView showsVerticalScrollIndicator={false}>
                <View style={{flexDirection:"row",marginBottom:10}}>
                    <CustomButton title={'Salons'}  buttons={Style.ButtonTime}                 
                    />
                    <TouchableOpacity style={{marginTop:10}}>
                    <CustomText style={[Typography.heading,{fontSize:wp('4.6%'),color:PrimaryTheme.$BACKGROUND_COLOR}]}>
                             Stylists</CustomText>
                    </TouchableOpacity>
                </View>
                <Favoriteslist />
                <Favoriteslist />
                <Favoriteslist />
                <Favoriteslist />
                <Favoriteslist />
            </ScrollView>
            </Container>
    )
}
const Style = StyleSheet.create({
    ButtonTime:{
      backgroundColor:PrimaryTheme.$BACKGROUND_COLOR,
      width:wp('30%'),
      borderRadius: 10,
      height: hp('6.5%'),
      fontSize:14,
      padding:9,
      letterSpacing:1, 
      marginRight:10
  },
    })
export default FavoritesScreen;


{/* <View style={{flexDirection:"row",justifyContent:"space-between"}}>
<View style={{flexDirection:"row"}}>
 <Image style={{width:wp('24%'),height:hp('12%')}} 
     resizeMode={'contain'} source={Utils.image.OrangeTheSalon} 
 />
<View>

<CustomText style={[Typography.heading,{fontSize:wp('3.7%'),color:PrimaryTheme.$Black,marginBottom:5}]}>Orange The Salon</CustomText>
 <CustomText style={[Typography.subheading,{fontSize:wp('3.5%'),color:PrimaryTheme.$Dim_Gray,}]}>Haircut  
 <CustomText style={[Typography.subheading,{fontSize:wp('3.5%'),color:PrimaryTheme.$Dim_Gray,}]}>   Hair Spa </CustomText>
 </CustomText>
 <CustomText style={[Typography.lighText,,{fontSize:13,marginTop:20}]}>Sept 19 10 : 00 AM</CustomText>
</View>
</View>
 
     <View style={{justifyContent:"space-between"}}>
         <Text>
     <Icon name={'star'} size={14} style={{}}
    color={PrimaryTheme.$BACKGROUND_COLOR}
     /> 
     4.5
     </Text>
         <View style={{flexDirection:"row"}}>
         <View style={{marginRight:10}}>
         <Icon name={'star'} size={14} style={{}}
    color={PrimaryTheme.$BACKGROUND_COLOR}
     /> 
         </View>
      <Icon name={'star'} size={14} style={{}}
    color={PrimaryTheme.$BACKGROUND_COLOR}
     /> 
         </View>
   
     </View>
</View> */}