import * as React from 'react';
import {FlatList, Text, View,Image} from 'react-native';
import Gallery from 'react-native-image-gallery';
import Container from '../component/Container';
import { Utils } from '../utills/utils';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

interface Props{
  
}

const Viewlist = (props:Props) =>{
  const DATA = [
    {
      id: "1",
      title: "1",
      image:
        "https://images.pexels.com/photos/3800060/pexels-photo-3800060.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500"
    },
    {
      id: "2",
      title: "2",
      image:
        "https://images.pexels.com/photos/324655/pexels-photo-324655.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500"
    },
    {
      id: "3",
      title: "3",
      image:
        "https://images.pexels.com/photos/3992870/pexels-photo-3992870.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500"
    },
    {
      id: "4",
      title: "4",
      image:
        "https://images.pexels.com/photos/3993310/pexels-photo-3993310.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500"
    },
    {
      id: "5",
      title: "5",
      image:
        "https://images.pexels.com/photos/2301842/pexels-photo-2301842.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500"
    },
    {
      id: "6",
      title: "6",
      image:
        "https://images.pexels.com/photos/1321916/pexels-photo-1321916.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500"
    },
    {
      id: "7",
      title: "7",
      image:
        "https://images.pexels.com/photos/1021693/pexels-photo-1021693.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500"
    },
    {
      id: "8",
      title: "8",
      image:
        "https://images.pexels.com/photos/730055/pexels-photo-730055.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940"
    }
  ];
  const  renderItem = ({ item }) => (
    <View style={{ flex: 1, marginHorizontal: 15, marginBottom: 10 }}>
      <Image
        style={{ width: wp('45%'), height: hp('20%') }}
        source={{ uri: item.image }}
      />
      <Text style={{ textAlign: "center", marginTop: 8 }}>{item.title}</Text>
    </View>
  );
    return(
        <Container ContainerStyle={{justifyContent:"flex-start",}}> 
           <FlatList showsVerticalScrollIndicator={false}
          data={DATA}
          renderItem={renderItem}
          keyExtractor={(item,index) => index.toString()}
          numColumns={2}
          style={{ flex: 1 }}
          contentContainerStyle={{ paddingVertical: 20 }}
        />
        </Container>
    )
}

export default Viewlist;
