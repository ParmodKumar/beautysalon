import * as React from 'react';
import {View,Image,StyleSheet,Text,ScrollView} from 'react-native';
import Container from '../component/Container';
import { Utils } from '../utills/utils';
import CustomText from '../component/CustomText';
import { Spaning, Typography } from '../style/Global';
import CustomButton from '../component/CustomButton';
import { PrimaryTheme } from '../style/Theme';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { ScreenName } from '../utills/Navigations/Routes';
import { TextInput } from 'react-native-paper';

interface Props{
    route:any;
    navigation:any
}

const VerifyAccount = (props:Props) =>{

    const HairStyling = () =>{
        props.navigation.navigate(ScreenName.HAIRSTYLING)
    }
    return(
        <ScrollView showsVerticalScrollIndicator={false} >
        <Container ContainerStyle={{justifyContent:"flex-start",alignItems:"center",marginHorizontal:15}}>
           
            <CustomText style={[Typography.title,
                {marginTop:Spaning.small.marginTop,textAlign:"center"}]}>
                    Verify Account</CustomText>
                    <Image resizeMode={'contain'} style={Style.Verifyimg} source={Utils.image.Verify} />
                    <View style={{}}>
                        <CustomText style={[Typography.paragraph,{marginBottom:Spaning.small.marginBottom}]}>
                            Enter four digit code sent on your mobile number.
                        </CustomText>
                        <Text>HELLO</Text> 
                        <View>
                       
                        </View>
                        <CustomButton title={'Verify OTP'}  buttons={Style.ButtonSingin}
                              onPress={HairStyling}
                        />
                        <CustomText  style={[Typography.paragraph,{textAlign:"center"}]}>
                        Did not receive OTP? <CustomText style={[Typography.paragraph,{color:PrimaryTheme.$BACKGROUND_COLOR,fontWeight:"bold"}]}>Resend OTP</CustomText>
                        </CustomText>
                    </View> 
                
         </Container>
         </ScrollView>    
    )
}

export default VerifyAccount;

const Style = StyleSheet.create({
    Verifyimg:{
        width: wp('80%'),
        height: hp('40%'),
        borderColor:"red",
        borderRadius: 10, 
        marginBottom:Spaning.small.marginBottom,
    },
    ButtonSingin:{
        backgroundColor:PrimaryTheme.$BACKGROUND_COLOR,
        marginBottom:Spaning.tiny.marginBottom,
       color:PrimaryTheme.$TEXT_COLOR,
       borderRadius: 10,
      },
})