import React, { useState, useEffect } from 'react';
import { Text, View, Animated, StyleSheet, TouchableOpacity, FlatList, Image, ImageBackground, Dimensions, CheckBox } from 'react-native';
import { Calendar } from 'react-native-calendars';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { images } from '../utills/Date';
import ImageOverlay from "react-native-image-overlay";
import { PrimaryTheme } from '../style/Theme';
import CustomText from '../component/CustomText';
import { Spaning, Typography } from '../style/Global';
import Container from '../component/Container';
import Icon from '../component/Icon';
import CustomButton from '../component/CustomButton';
import { Utils } from '../utills/utils';
import { List } from 'react-native-paper';
import Saloncall from '../component/Saloncall';

interface Props {
 id?:any,
 index?:any
}
const BookingScreen = (props:Props) => {
    return (
      <>
        <View style={{backgroundColor:"#b0ddf0",}}>
          <View style={{flexDirection:"row",marginHorizontal:15,marginTop:10,marginBottom:10}}>
              <View style={{marginRight:10}}>
                <Image style={{width:wp('20%'),height:hp('12%')}} resizeMode={'contain'} source={Utils.image.Margaret} 
                />
              </View>
          <View>
            <CustomText style={[Typography.heading,{fontSize:wp('4.6%'),color:PrimaryTheme.$Black,marginBottom:5}]}>Margaret Oliver</CustomText>
            <CustomText style={[Typography.subheading,{fontSize:wp('3.8%'),color:PrimaryTheme.$Dim_Gray,marginBottom:5}]}>marget.oliver@gmail.com</CustomText>
          <Text style={[]}>123 456 7890</Text>
          </View>
          </View>
          </View>
      <Container ContainerStyle={{justifyContent:"flex-start",marginHorizontal:15,
          marginTop:Spaning.small.marginTop,}}>  
                <View style={{flexDirection:"row"}}>
                    <CustomButton title={'Upcoming'}  buttons={Style.ButtonTime}                 
                    />
                    <TouchableOpacity style={{marginTop:10}}>
                    <CustomText style={[Typography.heading,{fontSize:wp('4.6%'),color:PrimaryTheme.$BACKGROUND_COLOR}]}>Previous</CustomText>
                    </TouchableOpacity>
         </View>
         <Saloncall 
        img={Utils.image.OrangeTheSalon}
        title='Orange The Salon'
        title1='Haircut'
        title2='Hair Spa'
        time='Sept 19 10 : 00 AM'
         />
           <Saloncall 
        img={Utils.image.EnhanceSalon}
        title='Enhance Salon'
        title1='Oil Massage'
        time='Sept 21 10 : 00 AM'
         />
      </Container>
      </>
    );
  }; 
const Style = StyleSheet.create({
  ButtonTime:{
    backgroundColor:PrimaryTheme.$BACKGROUND_COLOR,
    width:wp('30%'),
    borderRadius: 10,
    height: hp('6.5%'),
    fontSize:14,
    padding:9,
    letterSpacing:1, 
    marginRight:10
},
  })
export default BookingScreen;









{/* <View style={{flexDirection:"row"}}>
<TouchableOpacity style={{height: hp('6%'),flex:1,backgroundColor:"red",
        borderRadius: 6,  borderWidth:1,
        borderColor:'#fff',
        alignItems:'center',
        flexDirection:'row',
        justifyContent:'center',}}
       
>
<Text>click</Text>
</TouchableOpacity>
</View> */}








{/* <View style={Style.body}>
<Text style={Style.text}>
    Screen A
</Text>
<Pressable
    onPress={()=>{}}
    style={({ pressed }) => ({ backgroundColor: pressed ? '#ddd' : '#0f0' })}
>
    <Text style={Style.text}>
        Go to Screen B
</Text>
</Pressable>
</View> */}

{/* <Container ContainerStyle={{justifyContent:"flex-start",marginHorizontal:15,}}>
<View style={{flexDirection:"row"}}>
<Image style={{width: wp('50%'),height: hp('10%'),flex:1,marginRight:10,}}
source={Utils.image.Background} /> 
 <Image style={{width: wp('50%'),height: hp('10%'),flex:1}}
source={Utils.image.Background} /> 
</View>
</Container> */}


