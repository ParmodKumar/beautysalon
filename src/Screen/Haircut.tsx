import * as React from 'react';
import {Text, View,Image,StyleSheet, ScrollView, TouchableOpacity} from 'react-native';
import Container from '../component/Container';
import CustomText from '../component/CustomText';
import { Typography } from '../style/Global';
import { Utils } from '../utills/utils';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { PrimaryTheme } from '../style/Theme';
import Icon from '../component/Icon';
import { color } from 'react-native-reanimated';
import { ScreenNames } from './HairStyling';
import Solons from '../component/Salons';
import { useOrientation } from '../utills/useOrientation';

interface Props{
    route:any;
    navigation:any
  }

const Haircut = (props:Props) =>{
    const SalonDelailClick = () =>{
        props.navigation.navigate(ScreenNames.SALONDELAIL)
      }

    return(
        <Container ContainerStyle={{justifyContent:"flex-start",marginHorizontal:15,}}>
             <ScrollView showsVerticalScrollIndicator={false}>
                <CustomText style={[Typography.title,{fontWeight:'900',}]}>Salons</CustomText>
                <Solons 
                    onPress={SalonDelailClick}
                    imges={Utils.image.OrangeSalon}
                    heading='Orange The Salon'
                    headingnum='4.5'
                    subheading='Opp Courtyard Marriot, Satelite'
                    subheadingkm='1.2 km'
                    subheadingnum='800'
                    Service='Service'
                />
                  <Solons 
                onPress={SalonDelailClick}
                imges={Utils.image.MonarchSalon}
                heading='Monarch Hair And Beauty Salon'
                headingnum='4.5'
                subheading='Shop 2, Shukan 2, Opposite'
                subheadingkm='1.2 km'
                subheadingnum='800'
                Service='Service'
                />
                </ScrollView>
        </Container>
    )
}

const Style = StyleSheet.create({
    HairServicesimg:{
        width: wp('91.6%'),
        height: hp('30%'),
        borderRadius: 5,
        marginBottom:10,
        marginTop:4,
      
    },
    Salonsheading:{
        color:PrimaryTheme.$Black,
        fontSize:12.3
    },
    Salonssubheading:{
        color:PrimaryTheme.$Dim_Gray,
        fontSize:12.3
    }
})
export default Haircut;




// <View>
// <TouchableOpacity onPress={SalonDelailClick} activeOpacity={0.8}>
// <Image 
//     source={Utils.image.OrangeSalon}
//     style={Style.HairServicesimg}
// />
// </TouchableOpacity>
// <View style={{flexDirection:"row",justifyContent:"space-between"}}>
//     <CustomText style={[Typography.heading,Style.Salonsheading]}>Orange The Salon</CustomText>
//     <View style={{flexDirection:"row"}}>
//     <Icon name={'star'} size={16} style={{marginRight:5,}}
//             color={PrimaryTheme.$BACKGROUND_COLOR}
//         />  
//    <CustomText style={[Typography.heading,Style.Salonsheading]}>4.5</CustomText>
//     </View>
// </View>
// <View style={{flexDirection:"row",marginBottom:2,justifyContent:"space-between"}}>
//    <View style={{}}>
//    <CustomText style={[Typography.subheading,Style.Salonssubheading]}>Opp Courtyard Marriot, Satelite</CustomText>
//    </View>
//    <CustomText style={[Typography.subheading,Style.Salonssubheading]}>1.2 km</CustomText>
// </View>
// <CustomText style={[Typography.subheading,Style.Salonssubheading]}>₹ 
// <CustomText style={[Typography.subheading,Style.Salonssubheading,{color:PrimaryTheme.$BACKGROUND_COLOR}]}> 800</CustomText> / Service</CustomText>
// <Image 
//     source={Utils.image.Monarch}
//     style={Style.HairServicesimg}
// />
//  {/* <View style={{flexDirection:"row",marginBottom:2,}}>
//    <View style={{width: wp('80%'),}}>
//         <CustomText style={[Typography.heading,{color:PrimaryTheme.$Black,fontSize:12.3}]}>Monarch Hair And Beauty Salon</CustomText>
//    </View>
//    <Icon name={'star'} size={16} style={{marginRight:2,}}
//       color={PrimaryTheme.$BACKGROUND_COLOR}
//   />  
//    <CustomText style={[Typography.heading,{color:PrimaryTheme.$Black,fontSize:12.3}]}>5.0</CustomText>
// </View> */}

// <View style={{flexDirection:"row",justifyContent:"space-between"}}>
//     <CustomText style={[Typography.heading,Style.Salonsheading]}>Monarch Hair And Beauty Salon</CustomText>
//     <View style={{flexDirection:"row"}}>
//     <Icon name={'star'} size={16} style={{marginRight:5,}}
//             color={PrimaryTheme.$BACKGROUND_COLOR}
//         />  
//    <CustomText style={[Typography.heading,Style.Salonsheading]}>5.0</CustomText>
//     </View>
// </View>
// <View style={{flexDirection:"row",marginBottom:2,justifyContent:"space-between"}}>
//    <View style={{}}>
//    <CustomText style={[Typography.paragraph,Style.Salonssubheading]}>Shop 2, Shukan 2, Opposite Nirgun Bunglow,{"\n"}
//     Near RH Kapdiya School, Karnavati Club Road.</CustomText>
//    </View>
//    <CustomText style={[Typography.subheading,Style.Salonssubheading]}>1.0 km</CustomText>
// </View>
// <CustomText style={[Typography.subheading,Style.Salonssubheading]}>₹ 
// <CustomText style={[Typography.subheading,Style.Salonssubheading,{ color:PrimaryTheme.$BACKGROUND_COLOR,
// }]}> 1000</CustomText> / Service</CustomText>
// </View>