import * as React from 'react';
import {Text,TextInput,StyleSheet, View,Image,TouchableOpacity,ScrollView,Platform,StatusBar} from 'react-native';
import Container from '../component/Container';
import {widthPercentageToDP as wp, heightPercentageToDP as hp, listenOrientationChange, removeOrientationListener} from 'react-native-responsive-screen';
import { PrimaryTheme } from '../style/Theme';
import CustomText from '../component/CustomText';
import { Typography } from '../style/Global';
import { Utils } from '../utills/utils';
import CustomButton from '../component/CustomButton';
import { Formik } from 'formik';
import {Spaning} from '../style/Global'
import { Validator } from '../utills/Validator';
import If from '../component/If';
import axios from 'axios'
interface Props{
}
interface State{
    form:{
        NameInuputValue:string,
        emailInuputValue:string,
        PasswordInuputValue:string,
   },
      orientation : string
}

class SingUp extends React.Component<Props,State>{
    private EamilInputref;
    private PasswordRef;
    constructor(props){
        super(props);
        this.state = {
            form:{
                NameInuputValue:"",
                emailInuputValue:"",
                PasswordInuputValue:"",
            },
            orientation:"portrait"
          }
    }
    handlogoing = ()=>{
        alert('hello')
    }
    componentDidMount(){
        listenOrientationChange(this)
      }
      componentWillUnmount(){
        removeOrientationListener();
      }
  
      
      
      
    render(){
        return(  
            
            <Container ContainerStyle={{justifyContent:"flex-start",marginHorizontal:15}}> 
          
            <ScrollView showsVerticalScrollIndicator={false}>
           
                <CustomText style={[Typography.title,{marginTop:Spaning.larg.marginTop,textAlign:"center"}]}>Create Account</CustomText>
                   <Formik 
                        validateOnChange={true}
                        validationSchema={Validator.Longvalidation}
                        initialValues={this.state.form} 
                        validateOnMount={true}
                        onSubmit={()=>{
                            const userObject = {
                                NameInuputValue: this.state.form.NameInuputValue,
                                emailInuputValue: this.state.form.emailInuputValue,
                                PasswordInuputValue: this.state.form.PasswordInuputValue,
                            };
                    
                            axios.post('http://localhost:4000/users/create', userObject)
                                .then((res) => {
                                    console.log(res.data)
                                }).catch((error) => {
                                    console.log(error)
                                });
                        }}
                   >
                   {(props)=>{
                       return(
                       <>
                        <TextInput  
                            onSubmitEditing={() => this.EamilInputref.focus()}
                            returnKeyType={"next"}  
                            style={Style.inputstyle}
                            onBlur={()=>props.setFieldTouched('NameInuputValue')}
                            placeholder={'Name'}
                            onChangeText={props.handleChange('NameInuputValue')}
                            value={props.values.NameInuputValue}
                           
                        />
                        <If show={props.dirty && props.touched.NameInuputValue}>
                            <CustomText style={[Typography.errorText]}>{props.errors.NameInuputValue}</CustomText>
                        </If>
                        <TextInput  
                            onBlur={()=>props.setFieldTouched('emailInuputValue')}
                            returnKeyType={"next"}  
                            ref={ref => this.EamilInputref = ref}
                            onSubmitEditing={()=>this.PasswordRef.focus()}
                            style={Style.inputstyle}
                            placeholder={'Email'}
                            onChangeText={props.handleChange('emailInuputValue')}
                            value={props.values.emailInuputValue}
                        />
                        <If show={props.dirty && props.touched.emailInuputValue}>
                             <CustomText style={[Typography.errorText]}>{props.errors.emailInuputValue}</CustomText>
                        </If>
                        <TextInput  onSubmitEditing={()=>{
                            if(props.isValid){
                               console.log('isValid ')
                            }else{
                                console.log('is from Valid ')
                            }
                        }}
                            onBlur={()=>props.setFieldTouched('PasswordInuputValue')}
                            returnKeyType={'done'}
                            ref={ref => this.PasswordRef = ref}
                            style={Style.inputstyle}
                            placeholder={'Password'}
                            value={props.values.PasswordInuputValue}
                            onChangeText={props.handleChange('PasswordInuputValue')}
                        />  
                        <If show={props.dirty && props.touched.PasswordInuputValue}>     
                            <CustomText style={[Typography.errorText]}>{props.errors.PasswordInuputValue}</CustomText>
                        </If> 
                        <View style={{marginTop:Spaning.larg.marginTop}}>
                                <View style={{}}>
                                    <CustomText style={[Typography.lighText,{marginBottom:Spaning.extrlarg.marginBottom}]}>Gender</CustomText>
                                       <View style={[Style.parent]}>
                                            
                                                   
                                                    <TouchableOpacity  style={Utils.dynamStyle(
                                                           SingUportrait.borderImg,SingUlandscape.borderImg , 
                                                           this.state.orientation
                                                        )}>   
                                                        <Image style={Utils.dynamStyle(
                                                           SingUportrait.Genderimg,SingUlandscape.Genderimg , 
                                                           this.state.orientation
                                                        )} source={Utils.image.Gender} /> 
                                                    </TouchableOpacity>
                                                
                                          
                                               
                                                    <TouchableOpacity style={Utils.dynamStyle(
                                                           SingUportrait.borderImg,SingUlandscape.borderImg , 
                                                           this.state.orientation
                                                        )}>   
                                                        <Image style={Utils.dynamStyle(
                                                           SingUportrait.Genderimg,SingUlandscape.Genderimg , 
                                                           this.state.orientation
                                                        )} source={Utils.image.Gender1} /> 
                                                    </TouchableOpacity>
                                         </View>
                                </View>
                          <View style={{alignItems:"center"}}>
                          <CustomButton title={'Sign Up'}  buttons={Style.ButtonSingin}
                                onPress={()=>{
                                    if(props.isValid){
                                        console.log('isValid ')
                                        props.handleSubmit();
                                    }else{
                                        console.log('is from Valid ',props.errors)
                                    }
                                }} 
                            />
                          </View>
                        </View>
                    </>
                       )
                   }}
                </Formik>
              
                </ScrollView>
            </Container>
           
        )
    }    
} 
export default SingUp;
    const Style  = StyleSheet.create({
        inputstyle:{
            // width:wp('90%'),
            borderBottomColor:PrimaryTheme.$light_silver,
            borderBottomWidth: 1.4,
            marginBottom: 4,
            fontSize:16,
            letterSpacing:0.3, 
        },
        // Genderimg:{
        //     marginTop:Spaning.larg.marginTop,
        //     width: wp('32%'),
        //     height: hp('18%'),
        //     alignItems:'center',
        //     borderColor:"red",
        //     borderRadius: 10,
        //     marginBottom:Spaning.larg.marginBottom,
        //    flex:1
        // },
        // borderImg:{
        //     borderWidth:1,
        //     width: wp('43%'),
        //     alignItems:"center",
        //     borderColor:PrimaryTheme.$Suva_Grey,
        //     marginBottom:Spaning.extrlarg.marginBottom,
        //     borderRadius: 10,
        // },
        ButtonSingin:{
            backgroundColor:PrimaryTheme.$BACKGROUND_COLOR,
            marginBottom:Spaning.extrlarg.marginBottom,
            color:PrimaryTheme.$TEXT_COLOR,
            borderRadius: 10,  
        },
        parent: {
          
            flexDirection: 'row', 
          
        },
        child: {
           
            margin: '1%', 
         
        }
     
})

const  SingUportrait = StyleSheet.create({
    Genderimg:{
        marginTop:Spaning.larg.marginTop,
        width: wp('32%'),
        height: hp('18%'),
        borderColor:"red",
        borderRadius: 10,
        marginBottom:Spaning.larg.marginBottom,
       flex:1,
      
    },
    borderImg:{
        borderWidth:1,
        width: wp('43.5%'),
        alignItems:"center",
        borderColor:PrimaryTheme.$Suva_Grey,
        marginBottom:Spaning.extrlarg.marginBottom,
        borderRadius: 10,
        marginRight:15
    },
})
const  SingUlandscape = StyleSheet.create({
    Genderimg:{
        ...SingUportrait.Genderimg,  width: wp('42%'), height: hp('25%'),
    },
    borderImg:{
        ...SingUportrait.borderImg,
        width: wp('81%'),
        
    },
})
 



{/* <View style={{width:wp('80%')}}> */}


                                        {/* <View style={{flexDirection:"row"}}>
                                                <View >
                                                    <TouchableOpacity  style={Style.borderImg}>   
                                                        <Image style={Style.Genderimg} source={Utils.image.Gender} /> 
                                                    </TouchableOpacity>
                                                </View>
                                                <View style={{marginLeft:Spaning.extrlarg.marginLeft}}> 
                                                    <TouchableOpacity style={Style.borderImg}>   
                                                        <Image style={Style.Genderimg} source={Utils.image.Gender1} /> 
                                                    </TouchableOpacity>
                                                </View>
                                        </View> */}