import * as React from 'react';
import {View,Image,StyleSheet,StatusBar, ScrollView,Text, Dimensions, useWindowDimensions} from 'react-native';
import Container from '../component/Container';
import ImageOverlay from "react-native-image-overlay";
import { Utils } from '../utills/utils';
import CustomText from '../component/CustomText';
import { landscapeTypography, Spaning, Typography } from '../style/Global';
import CustomButton from '../component/CustomButton';
import { PrimaryTheme } from '../style/Theme';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { ScreenName } from '../utills/Navigations/Routes';
import { useOrientation } from '../utills/useOrientation';

interface Props{
  route:any;
  navigation:any
}

const SigninandSignUp = (props:Props) =>{
    const SingInClick = () =>{
        props.navigation.navigate(ScreenName.SINGIN)
    }
    const SingUpClick = () =>{
        props.navigation.navigate(ScreenName.SINGUP)
    }
     const orientation = useOrientation();
    return(
        <Container ContainerStyle={{marginHorizontal:0,}}> 
             <StatusBar backgroundColor={PrimaryTheme.$BACKGROUND_COLOR} />
              <ScrollView showsVerticalScrollIndicator={false}> 
                  <View style={orientation === 'portrait' ? 
                  portraitImageOverlay.ImageOverlaysec :  landscapeImageOverlay.ImageOverlaysec}>
                <ImageOverlay 
                  containerStyle={orientation === 'portrait' ? 
                  portraitImageOverlay.image :  landscapeImageOverlay.image}
                  source={Utils.image.Background}
                  overlayColor={PrimaryTheme.$BACKGROUND_COLOR}
                  overlayAlpha={0.7}
                >
                <View style={{alignItems:"center",flex:1}}>
                    <Image resizeMode="cover" style={orientation === 'portrait' ? 
                      portraitImageOverlay.logo :  landscapeImageOverlay.logo} source={Utils.image.Logoimg}
                />  
             <View style={{}}>
              <CustomText style={[{textAlign:"center",}, orientation === 'portrait' ? 
                  Typography.extrlarg : landscapeTypography.extrlarg]}
                >Book an Appointment for Salon,Spa & Barber.</CustomText>
             </View>
             <View style={{}}>
              <CustomButton  title={'Sing In'} buttons={orientation === 'portrait' ? 
              portraitImageOverlay.ButtonSingin :  landscapeImageOverlay.ButtonSingin} 
              textstyle={{color:PrimaryTheme.$BACKGROUND_COLOR}}
                onPress={SingInClick} />
            <CustomButton  onPress={SingUpClick} title={'Sing Up'} 
            textstyle={{color:PrimaryTheme.$BACKGROUND_COLOR}}/> 
            </View>
          </View>
          </ImageOverlay>
             </View>
          </ScrollView>
         </Container>
    )
}

export default SigninandSignUp;
    const portraitImageOverlay = StyleSheet.create({
        image: {
         height:hp('100%'),
          // flexDirection: 'row',
          flex:1
        },
        ImageOverlaysec:{
        },
        ButtonSingin:{
          backgroundColor:PrimaryTheme.$TEXT_COLOR,
          marginBottom:Spaning.tiny.marginBottom,
          color:PrimaryTheme.$BACKGROUND_COLOR,
        },
        logo:{
          marginTop:hp("25%"),
          height: hp('20%'),
          width: wp('35%'), 
        },
      })

  const landscapeImageOverlay = StyleSheet.create({
    image: {
      ...portraitImageOverlay.image,  
      height:hp('53.5%'),
    },
    ButtonSingin:{
      ...portraitImageOverlay.ButtonSingin,
    },
    logo:{
      ...portraitImageOverlay.logo,marginTop:20,
      width: wp('30%'),
    },
    ImageOverlaysec:{
      flexDirection: "row"
    },
  })
  // aspectRatio: 1 ,height: "auto",


  // <Image resizeMode="cover" style={orientation === 'portrait' ? 
  // portraitImageOverlay.logo :  landscapeImageOverlay.logo} source={Utils.image.Logoimg}/>  


 