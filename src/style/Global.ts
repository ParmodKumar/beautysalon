
import * as React from 'react'
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
import { widthPercentageToDP as wp,heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { PrimaryTheme } from '../style/Theme';


export const Typography : any = {

   
    title:{
        fontSize:wp('5.5%'),
        fontWeight:"600",
        fontFamily:'Lato',
        marginBottom: wp("2%"),
        color:PrimaryTheme.$Black,
        
    },
    heading:{
        fontSize:wp('8%'),
        fontWeight:"bold",
        fontFamily:'Lato',
    },
    subheading:{
        fontSize:wp('8%'),
        fontWeight:"600",
        fontFamily:'LatoItalic',
        color:PrimaryTheme.$TEXT_COLOR,
    },
    paragraph:{
        fontSize:wp('4.5%'),
        fontFamily:'Lato',
        color:PrimaryTheme.$Dim_Gray,
    },
    lighText:{
        fontFamily:'Lato',
        color:PrimaryTheme.$Black,
        fontSize:15,
        marginBottom:5
    },
    errorText:{
        fontSize:wp('3.5%'),
        fontFamily:'Muli',
        color:PrimaryTheme.$Red
    },
    extrlarg:{
        fontSize:wp('7.3%'),
        fontWeight:"600",
        fontFamily:'LatoItalic',
        color:PrimaryTheme.$TEXT_COLOR,
        marginBottom:wp('10%')
    }
}


export const landscapeTypography : any = {
    title:{
        fontSize:wp('6%'),
        fontWeight:"600",
        fontFamily:'Lato',
        marginBottom: wp("2%"),
        color:PrimaryTheme.$Black,
    },
    heading:{
        fontSize:wp('8%'),
        fontWeight:"bold",
        fontFamily:'Lato',
    },
    subheading:{
        fontSize:wp('8%'),
        fontWeight:"600",
        fontFamily:'LatoItalic',
        color:PrimaryTheme.$TEXT_COLOR,
    },
    paragraph:{
        fontSize:wp('4.5%'),
        fontFamily:'Lato',
        color:PrimaryTheme.$Dim_Gray,
    },
    lighText:{
        fontFamily:'Lato',
        color:PrimaryTheme.$Black,
        fontSize:15,
        marginBottom:5
    },
    errorText:{
        fontSize:wp('3.5%'),
        fontFamily:'Muli',
        color:PrimaryTheme.$Red
    },
    extrlarg:{
        fontSize:wp('7.3%'),
        fontWeight:"600",
        fontFamily:'LatoItalic',
        color:PrimaryTheme.$TEXT_COLOR,
        marginBottom:wp('5%')
    }
}






// -----------------------Spaning-----------------------------------------

export const Spaning = {
    // 5
   tiny:{
     marginLeft: wp("1.5%"),
     marginRight:wp("1.5%"),
     marginTop: hp("1%"),
     marginBottom: hp("1.5%"),
   },
   // 10
   small:{
       marginLeft: wp("0.5%"),
       marginRight:wp("0.5%"),
       marginTop: hp("3%"),
       marginBottom: hp("3%"),
   },
   // 25
   regural:{
       marginLeft: wp("1%"),
       marginRight:wp("5%"),
       marginTop: hp("1%"),
       marginBottom: hp("3.5%"),
   },
   larg:{
       marginLeft: wp("1.5%"),
       marginRight:wp("1.5%"),
       marginTop: hp("2%"),
       marginBottom: hp("1.5%"),
   },
   // 30
   extrlarg:{
       marginLeft: wp("3%"),
       marginRight:wp("3%"),
       marginTop: hp("3%"),
       marginBottom: hp("4%"),
   },
}


export const landscapeSpaning = {
     // 5
    tiny:{
      marginLeft: wp("0.25%"),
      marginRight:wp("0.25%"),
      marginTop: hp("0.25%"),
      marginBottom: hp("1.5%"),
    },
    // 10
    small:{
        marginLeft: wp("0.5%"),
        marginRight:wp("0.5%"),
        marginTop: hp("3%"),
        marginBottom: hp("3%"),
    },
    // 25
    regural:{
        marginLeft: wp("1%"),
        marginRight:wp("5%"),
        marginTop: hp("1%"),
        marginBottom: hp("3.5%"),
    },
    larg:{
        marginLeft: wp("1.5%"),
        marginRight:wp("1.5%"),
        marginTop: hp("2%"),
        marginBottom: hp("1.5%"),
    },
    // 30
    extrlarg:{
        marginLeft: wp("3%"),
        marginRight:wp("3%"),
        marginTop: hp("3%"),
        marginBottom: hp("4%"),
    },
}




export const pComponentStyle = {
    Genderimg:{
        marginTop:Spaning.larg.marginTop,
        width: wp('30%'),
        height: hp('18%'),
        alignItems:'center',
        borderColor:"red",
        borderRadius: 10,
        marginBottom:Spaning.larg.marginBottom,
       
    },
} 
export const  lComponentStyle = {
    Genderimg:{
        ...pComponentStyle.Genderimg, width: wp('50%'), 
         },
} 


