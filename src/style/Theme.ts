export enum PrimaryTheme {
    $BACKGROUND_COLOR = '#0A5688',
    $TEXT_COLOR = "#fff",
    $light_silver  = '#D8D8D8',
    $Suva_Grey = '#909090',
    $Dim_Gray = '#686868',
    $Black = '#2E2E2E',
    $Red= 'red',
    }